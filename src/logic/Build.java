package logic;

import java.util.ArrayList;
import java.util.Iterator;

import NetworkBA_TD.Network;
import bankagent.IBShort;
import bankagent.Institution;

public class Build {

	private int banknode;
	private double megaCAR, mediumCAR, smallCAR, a1, a2, E, L, A0, asyCAR;
	String NW;
	String BankType;

	private double lbmin, lbmax;
	private double valb;
	private double vi;

	private double randIn, randOut, theta;
	public double ibrate;

	public double cardef;


	private int wdegsum = 0;
	private int degsumw=0;
	private int sumdeg = 0;//全枝数のカウント
	double sumex = 0;
	ArrayList<Institution> Bank = new ArrayList<Institution>();




	//Init
	public Build(int banknode, double E, double megaCAR, double mediumCAR, double smallCAR, double a1,
			double a2, String NW, String BankType, double A0, double asyCAR, double lbmin, double lbmax, double valb,
			double vi, double randIn, double randOut, double theta, double ibrate,double cardef) {
		this.banknode = banknode;
		this.E = E;
		this.L = E * ibrate;
		this.megaCAR = megaCAR;
		this.mediumCAR = mediumCAR;
		this.smallCAR = smallCAR;
		this.a1 = a1;
		this.a2 = a2;
		this.NW = NW;
		this.BankType = BankType;
		this.A0 = A0;
		this.asyCAR = asyCAR;
		this.lbmin = lbmin;
		this.lbmax = lbmax;
		this.valb = valb;
		this.vi = vi;
		this.randIn = randIn;
		this.randOut = randOut;
		this.theta = theta;
		this.ibrate = ibrate;
		this.cardef=cardef;
	}



	public void isBuild() {
		// TODO Auto-generated method stub

		//初期ネットワーク構築

		//BA
		/*
		if (NW == "BA") {
			Network BA = new Network(banknode, theta);
			BA.createBA();
			for (int i = 0; i < banknode; i++) {
				Institution FI = new Institution(i + 1, BA.getIndegree(i), BA.getOutdegree(i));//金融機関の入次数、出次数
				Bank.add(FI);
			}
			for (int i = 0; i < banknode; i++) {
				Bank.get(i).Inid = BA.getInid(i);// <arraylist>i番目の銀行の入ってきている銀行のidリスト
				Bank.get(i).Outid = BA.getOutid(i);//出て行ってい金融機関のid
			}
			//In,Outid=0をdelete
			for(Institution b:Bank) {
				Iterator<Integer> inid=b.Inid.iterator();
				while(inid.hasNext()) {
					Integer id=inid.next();
					if(id==0) inid.remove();
				}
				Iterator<Integer> outid=b.Outid.iterator();
				while(outid.hasNext()) {
					Integer id=outid.next();
					if(id==0) outid.remove();
				}
			}
			Deciderank();//自己資本比率の決定（総次数の大きさに応じて）
		}
		*/

		//CP
		/*
		if (NW == "CP") {

			NetworkCP CP = new NetworkCP(banknode, a1, a2, randIn, randOut);
			CP.createCP();

			NetworkCP1 CP = new NetworkCP1(banknode, a1, a2, randIn, randOut);
			CP.CreateCP1();
			for (int i = 0; i < banknode; i++) {
				Institution FI = new Institution(i+1, CP.getIndegree(i), CP.getOutdegree(i));//金融機関の入次数、出次数
				Bank.add(FI);
			}
			for (int i = 0; i < banknode; i++) {
				Bank.get(i).Inid = CP.getInid(i);// <arraylist>i番目の銀行の入ってきている銀行のidリスト
				Bank.get(i).Outid = CP.getOutid(i);//出て行ってい金融機関のid
			}
			CP.virtualClear();
			Deciderank();//自己資本比率の決定（総次数の大きさに応じて）
		}
	*/


		//完全ネットワーク
		/*
		if (NW == "CN") {
			Network CM = new Network(banknode, theta);
			CM.createCM();
			for (int i = 0; i < banknode; i++) {
				Institution FI = new Institution(i + 1, CM.getIndegree(i), CM.getOutdegree(i));//金融機関の入次数、出次数
				Bank.add(FI);
			}
			for (int i = 0; i < banknode; i++) {
				Bank.get(i).Inid = CM.getInid(i);// <arraylist>i番目の銀行の入ってきている銀行のidリスト
				Bank.get(i).Outid = CM.getOutid(i);//出て行ってい金融機関のid
			}
		}
		*/



		//BS構築(初期総資産べき則)のための重み作成
		//銀行エージェント作成
		for (int i = 0; i < banknode; i++) {
			Institution FI = new Institution(i + 1);//金融機関の入次数、出次数
			Bank.add(FI);
		}
		Network BAw = new Network(banknode, theta);
		BAw.createBA();
		for(int i=0;i<banknode;i++) {
			Bank.get(i).Indegw=BAw.getInid(i);
			Bank.get(i).Outdegw=BAw.getOutid(i);
		}
		for(Institution b:Bank) {
			Iterator<Integer> inidw=b.Indegw.iterator();
			while(inidw.hasNext()) {
				Integer id=inidw.next();
				if(id==0) inidw.remove();
			}
			Iterator<Integer> outidw=b.Outdegw.iterator();
			while(outidw.hasNext()) {
				Integer id=outidw.next();
				if(id==0) outidw.remove();
			}
		}
		Bank.stream().forEach(s->s.setSumdegw());


		//BScreate();//バランスシートの作成(e,n,d)
		BScreate1();//バランスシート作成(d)

		//LevListcreate();//leverageリストの作成

		for (int i = 0; i < banknode; i++) {
			Bank.get(i).state = "Alive";
		}
	}


	public void LevListcreate() {
		String state = "No";
		String range = "No";
		Bank.stream().forEach(s -> s.setLev(lbmin, lbmax, valb, state, range, vi));
	}

	private void BScreate1() {
		Bank.stream().forEach(s -> sumdeg += s.deg);
		//IB市場取引枝の設定
		IBdeg();

		//Asset全体の重み
		Bank.stream().forEach(s-> degsumw+=s.sumdegw);

		//BSCreate(d)
		Bank.stream().forEach(s->{
			s.totalassets=((double)s.sumdegw/degsumw)*E;
			s.totalinvest=s.totalassets;
			s.d=s.totalassets;
			s.buffer=s.totalassets;
		});
	}

	private void BScreate() {
		// TODO Auto-generated method stub

		Bank.stream().forEach(s -> sumdeg += s.deg);

		//IB市場取引枝の設定
		IBdeg();

		//他金融機関への短期コールローン、コールマネーの決定
		IBtranx();

		//cardの決定
		//Bank.stream().forEach(s->s.setCARD(cardef));

		//ibsの決定
		for (Institution b : Bank) {
			for (IBShort ib : b.ibsloan) {
				b.lb += ib.money;
			}
			for (IBShort ib : b.ibsborrow) {
				b.bb += ib.money;
			}
		}

		//e,d,nの決定
		/*
		for(Institution b:Bank) {
			if(b.bb-b.lb<0) { b.liqA=0;}else {
				b.liqA=b.bb-b.lb;
				sumex+=b.liqA;
			}
		}
		*/

		Bank.stream().forEach(s -> {
			s.totalassets = E * ((double) s.deg / (double) sumdeg);
			s.n = s.totalassets * s.CAR;
			s.d = s.totalassets - s.n - s.bb;
			s.buffer = s.totalassets - s.lb;
		});

	}

	private void IBtranx() {


		for (Institution b : Bank) {
			for (int i = 0; i < b.Inid.size(); i++) {
				IBShort ibs = new IBShort(b.Inid.get(i), L * b.cpbrin.get(i) / wdegsum);
				b.ibsborrow.add(ibs);
				b.ibsconnectedbank.add(b.Inid.get(i));
			}
			for (int i = 0; i < b.Outid.size(); i++) {
				IBShort ibs = new IBShort(b.Outid.get(i), L * b.cpbrout.get(i) / wdegsum);
				b.ibsloan.add(ibs);
			}
		}
	}


	private void IBdeg() {

		//IB取引額の重み
		for (Institution b : Bank) {
			for (int i = 0; i < b.Inid.size(); i++) {
				for (Institution bb : Bank) {
					if (b.Inid.get(i) == bb.id) {
						b.cpbrin.add(b.deg + bb.deg);
						wdegsum += b.deg + bb.deg;
					}
				}
			}
			for (int i = 0; i < b.Outid.size(); i++) {
				for (Institution bb : Bank) {
					if (b.Outid.get(i) == bb.id) {
						b.cpbrout.add(b.deg + bb.deg);
						wdegsum += b.deg + bb.deg;
					}
				}
			}
		}



	}

	private void Deciderank() {
		// TODO Auto-generated method stub

		for (int i = 0; i < banknode; i++) {
			if (i < banknode * a1) {
				Bank.get(i).CAR = megaCAR;
				Bank.get(i).forcelev = megaCAR;
				Bank.get(i).rank = "mega";
			}
			if (banknode * a1 <= i && i <= banknode * a2) {
				Bank.get(i).CAR = mediumCAR;
				Bank.get(i).forcelev = mediumCAR;
				Bank.get(i).rank = "medium";
			}
			if (banknode * a2 < i) {
				Bank.get(i).CAR = smallCAR;
				Bank.get(i).forcelev = smallCAR;
				Bank.get(i).rank = "small";

			}
		}
	}

	public void addExAsset1(double larate) {
		Bank.stream().forEach(s->{
			s.liqArate =larate;
			s.liqA = s.liqArate * s.totalassets;
			s.realliqA=s.liqA;
			s.buffer = s.totalassets - s.liqA;

		});
	}

	public void addExAsset(double la1, double la2) {
		// TODO 自動生成されたメソッド・スタブ

		//asset全体の保有市場性資産割合の決定
		Bank.stream().forEach(s -> {
			s.liqArate = la1 + Math.random() * (la2 - la1);
			s.liqA = s.liqArate * s.totalassets;
			s.realliqA=s.liqA;
			s.buffer = s.totalassets - s.liqA - s.lb;
		});

	}


	public ArrayList<Institution> getFinalBank() {
		// TODO Auto-generated method stub
		return Bank;
	}

}
