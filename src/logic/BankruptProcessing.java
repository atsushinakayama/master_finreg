package logic;

import java.util.ArrayList;
import java.util.Iterator;

import asset.Asset;
import bankagent.AssetList;
import bankagent.Institution;
import firmagent.Enterprise;

public class BankruptProcessing {

	ArrayList<Institution> bank = new ArrayList<Institution>();
	ArrayList<Institution> Sbank = new ArrayList<Institution>();//新規破綻行格納リスト
	ArrayList<Institution> DeadBank = new ArrayList<Institution>();//全破綻行リスト

	ArrayList<Asset> asset = new ArrayList<Asset>();
	ArrayList<Enterprise> firm= new ArrayList<Enterprise>();

	private double shockflucrate;
	public int fbanknum,fbankcar,fbankgap,fbankboth,fbankcm,fbankmega,fbankmedium,fbanksmall;

	public BankruptProcessing(ArrayList<Institution> bank, ArrayList<Asset> asset, double shockflucrate,ArrayList<Enterprise> firm) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.bank.addAll(bank);
		this.asset.addAll(asset);
		this.shockflucrate = shockflucrate;
		this.firm.addAll(firm);
	}

	public void isStart(double brCAR,double grossbadloan) {

		//状態の変更
		/*
		bank.stream().filter(s -> s.CAR < brCAR || s.shortage > 0.0001)
				.forEach(s -> s.brProcessing(brCAR));

		//破綻行リストに格納
		addBrBank();

		//propagate
		//path Asset
		setAssetShock();
		//path IBMarket
		for (Institution sb : Sbank) {
			for (Institution b : bank)
				grossbadloan-=b.setIBShock(sb.id,grossbadloan);
		}
		*/
		
		for(int i=0;i<100;i++) {
			propagateShock(brCAR);
			setOutPut();
			if(Sbank.size()==0) break;
		}
		

		//set firm BBlist 更新
		setFirmBBlist();
		//set output
		//setOutPut();
	}

	public void propagateShock(double brCAR) {
		double grossbadloan=0;
		Sbank.clear();
		//bank change state
		bank.stream().filter(s -> s.CAR < brCAR || s.shortage > 0.001)
					 .forEach(s -> s.brProcessing(brCAR));

		//add sbank list
		addBrBank();

		//propagate path I/B
		for (Institution sb : Sbank) {
			for (Institution b : bank)
				grossbadloan-=b.setIBShock(sb.id,grossbadloan);
		}

		//path Asset
		//asset shock は次のタームで算出
		setAssetShock();

		bank.stream().forEach(s -> s.setIBShortBS());
	}

	public void setAssetShock() {

		//保持している資産
		for (Institution sb : Sbank) {
			for (AssetList aslist : sb.Exasset)
				for (Asset as : asset) {
					if (aslist.id == as.id) {
						//as.setShockFlucrate(shockflucrate, aslist.amount);
						//shockrate 一定
						as.setShockFlucrateStatic(aslist.amount);
					}
				}
		}
		//グループ資産へのショック
		asset.stream().forEach(s->s.setByGroupFluc(asset));
	}

	public void addBrBank() {

		bank.stream().filter(s -> s.state == "Dead")
					 .forEach(s -> {
						 Sbank.add(s);
						 DeadBank.add(s);
					 });
		//bankリストから消去
		Iterator<Institution> b = bank.iterator();
		while (b.hasNext()) {
			Institution bb = b.next();
			if (bb.state == "Dead") {
				b.remove();
			}
		}

	}

	public void setFirmBBlist() {

		firm.stream().forEach(s->{
			for(Institution sb:Sbank) {
			s.setShorttermBB(sb.id);
			}
		});
	}

	public void setOutPut() {


		fbanknum=Sbank.size();
		if(Sbank.size()>0) {
		Sbank.stream().forEach(s->{
			switch(s.brfactor) {
			case "Both":
				fbankboth++;
				break;
			case "Buffer"	:
				fbankgap++;
				break;
			case "CAR":
				fbankcar++;
				break;
			};
			switch(s.rank) {
			case "mega":
				fbankmega++;
				break;
			case "medium":
				fbankmedium++;
				break;
			case "small":
				fbanksmall++;
				break;
			};
		});
		}
	}

	public void isStartAfterShortterm(double brCAR,double grossbadloan) {

		/*
		bank.stream().filter(s -> s.CAR < brCAR || s.shortage > 0.0001)
				.forEach(s -> s.brProcessing(brCAR));

		//破綻行リストに格納
		addBrBank();

		//propagate
		//path Asset
		setAssetShock();
		//path IBMarket
		for (Institution sb : Sbank) {
			for (Institution b : bank)
				grossbadloan-=b.setIBShock(sb.id,grossbadloan);
		}
	*/

		
		for(int i=0;i<100;i++) {
			propagateShock(brCAR);
			setOutPut1();
			if(Sbank.size()==0) break;
		}
		
		//set firm BBlist 更新
		setFirmBBlist();
		//set output
		//setOutPut1();
	}

	public void setOutPut1() {
		fbanknum=Sbank.size();
		fbankcm=Sbank.size();
		Sbank.stream().forEach(s->{
			switch(s.brfactor) {
			case "Both":
				fbankboth++;
				break;
			case "Buffer"	:
				fbankgap++;
				break;
			case "CAR":
				fbankcar++;
				break;
			};
			switch(s.rank) {
			case "mega":
				fbankmega++;
				break;
			case "medium":
				fbankmedium++;
				break;
			case "small":
				fbanksmall++;
				break;
			};
		});
	}

	public ArrayList<Institution> getBank() {
		// TODO 自動生成されたメソッド・スタブ
		return bank;
	}

	public ArrayList<Institution> getDeadBank() {
		return DeadBank;
	}



	public void AgentAndVariablesClear() {
		// TODO 自動生成されたメソッド・スタブ
		fbanknum=0;fbankboth=0;fbankgap=0;fbankcar=0;
		bank.clear();
		Sbank.clear();
		DeadBank.clear();
	}

	public int getFbanknum() {
		// TODO 自動生成されたメソッド・スタブ
		return fbanknum;
	}

	public int getFbankCAR() {
		// TODO 自動生成されたメソッド・スタブ
		return fbankcar;
	}

	public int getFbankBoth() {
		// TODO 自動生成されたメソッド・スタブ
		return fbankboth;
	}

	public int getFbankGap() {
		// TODO 自動生成されたメソッド・スタブ
		return fbankgap;
	}

	public int getFbankCM() {
		// TODO 自動生成されたメソッド・スタブ
		return fbankcm;
	}

	public int getFbankMega() {
		// TODO 自動生成されたメソッド・スタブ
		return fbankmega;
	}

	public int getFbankMedium() {
		// TODO 自動生成されたメソッド・スタブ
		return fbankmedium;
	}

	public int getFbankSmall() {
		// TODO 自動生成されたメソッド・スタブ
		return fbanksmall;
	}


}
