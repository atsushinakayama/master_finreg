package logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import NetworkBA_TD.Corres;
import NetworkBA_TD.Network;
import NetworkBA_TD.Virtual_BA;
import asset.Asset;
import bankagent.AssetList;
import bankagent.IBShort;
import bankagent.Institution;
import comparator.Bank_Sumdeg_BA_Comp;
import networkCP.NetworkCP1;

public class BuildComplete {

	public ArrayList<Institution> bank = new ArrayList<Institution>();
	public ArrayList<Asset> asset = new ArrayList<Asset>();
	public double bscallrate;

	public double assetsum;
	public double ibstock;
	public double wdegsum;
	public String nw;//network-type
	public double theta, randIn, randOut;//network TD weight
	public int m0;//BA init node num
	public int m;// BA add edge num
	public double a1, a2;//CAR銀行割り当て比率
	public double megaCAR,mediumCAR,smallCAR;
	public int la,megala,mediumla,smallla;
	public int degsum;

	//create network model of object
	ArrayList<Virtual_BA> v_ba = new ArrayList<Virtual_BA>();

	public void setInfo() {
		//bank totalasset合計の計算 IBmarket stock calc weightdegsum
		bank.stream().forEach(s -> assetsum += s.totalassets);
		ibstock = (bscallrate / (1 - bscallrate)) * assetsum;
	}

//// TD network consructor////////////////////////////////////////////////
	public BuildComplete(ArrayList<Institution> bank, ArrayList<Asset> asset, double bscallrate, String nw,
			double theta, double randIn, double randOut, double a1, double a2, int la, int megala, int mediumla, int smallla, double megaCAR, double mediumCAR, double smallCAR) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.bank.addAll(bank);
		this.asset.addAll(asset);
		this.bscallrate = bscallrate;
		this.nw = nw;
		this.theta = theta;
		this.randIn = randIn;
		this.randOut = randOut;
		this.a1 = a1;
		this.a2 = a2;
		this.la = la;
		this.megala=megala;
		this.mediumla=mediumla;
		this.smallla=smallla;
		this.megaCAR=megaCAR;
		this.mediumCAR=mediumCAR;
		this.smallCAR=smallCAR;
		setInfo();
	}


////BA network consructor////////////////////////////////////////////////

	public BuildComplete(ArrayList<Institution> bank, ArrayList<Asset> asset, double bscallrate, String nw,
			int m0, int m, double randIn, double randOut, double a1, double a2, int la, int megala,
			int mediumla, int smallla, double megaCAR, double mediumCAR, double smallCAR) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.bank.addAll(bank);
		this.asset.addAll(asset);
		this.bscallrate = bscallrate;
		this.nw = nw;
		this.m0 = m0;
		this.m = m;
		this.randIn = randIn;
		this.randOut = randOut;
		this.a1 = a1;
		this.a2 = a2;
		this.la = la;
		this.megala=megala;
		this.mediumla=mediumla;
		this.smallla=smallla;
		this.megaCAR=megaCAR;
		this.mediumCAR=mediumCAR;
		this.smallCAR=smallCAR;
		setInfo();
	}

//////////////////////////////////////////////////////////////////////////////////


	public void isStart() {

		//create network here
		createNW();

		//decide car ratio(rank)
		decideRank();

		//calc weightdegsum
		IBdeg();

		//call loan,moneyの決定
		IBtranx();

		//alloc exasset
		allocExasset();

		//adjust BS
		AdjustBS();

		//setCARD
		bank.stream().forEach(s->s.CARD = -10000);

	}



	public void decideRank() {

		for (int i = 0; i < bank.size(); i++) {
			if (i < bank.size() * a1) {
				bank.get(i).CAR = megaCAR;
				bank.get(i).forcelev = megaCAR;
				bank.get(i).rank = "mega";
			}
			if (bank.size() * a1 <= i && i <= bank.size() * a2) {
				bank.get(i).CAR = mediumCAR;
				bank.get(i).forcelev = mediumCAR;
				bank.get(i).rank = "medium";
			}
			if (bank.size() * a2 < i) {
				bank.get(i).CAR = smallCAR;
				bank.get(i).forcelev = smallCAR;
				bank.get(i).rank = "small";
			}
		}
	}

	public void createNW() {
		switch (nw) {
		case "TD":
			Network TD = new Network(bank.size(), theta);
			TD.createBA();
			for (int i = 0; i < bank.size(); i++) {
				bank.get(i).Inid = TD.getInid(i);// <arraylist>i番目の銀行の入ってきている銀行のidリスト
				bank.get(i).Outid = TD.getOutid(i);//出て行ってい金融機関のid
			}
			//In,Outid=0をdelete
			for (Institution b : bank) {
				Iterator<Integer> inid = b.Inid.iterator();
				while (inid.hasNext()) {
					Integer id = inid.next();
					if (id == 0)
						inid.remove();
				}
				Iterator<Integer> outid = b.Outid.iterator();
				while (outid.hasNext()) {
					Integer id = outid.next();
					if (id == 0)
						outid.remove();
				}
			}
			break;
		case "CP":
			/*
			NetworkCP1 CP = new NetworkCP1(bank.size(), a1, a2, randIn, randOut);
			CP.CreateCP1();
			for (int i = 0; i < bank.size(); i++) {
				bank.get(i).Inid = CP.getInid(i);// <arraylist>i番目の銀行の入ってきている銀行のidリスト
				bank.get(i).Outid = CP.getOutid(i);//出て行ってい金融機関のid
			}
			CP.virtualClear();
			*/
			break;

		case "BA":

			//Hashimoto model using BitSet

			Network BA = new Network(bank.size());
			int h = 0;
			do{
				BA.clear();
				h=0;
				BA.createKE2(m0, m, 5, 0);
				//if(BA.getNum_of_sumdeg());

				for(int i=0;i<bank.size();i++){
					if(BA.getOutdegree(i)==0 || BA.getIndegree(i)==0){
						h++;
					}
				}
			}while(h!=0);

			for(int i=0 ; i< bank.size() ; i++) {
				Virtual_BA v = new Virtual_BA(i+1,BA.getInid(i),BA.getOutid(i));
				v_ba.add(v);
			}

			//v_ba listを総次数で降順に
			Collections.sort(v_ba, new Bank_Sumdeg_BA_Comp());

			//現在のbankとvbaのid対応リストを作る
			ArrayList<Corres> crs = new ArrayList<Corres>();
			for(int i = 0 ; i < bank.size() ; i++) {
				Corres cor = new Corres(bank.get(i).id , v_ba.get(i).id);
				crs.add(cor);
			}

			//v_ba のそれぞれのリストに対応リストを照合させ，newリストを作る
			for(Virtual_BA vba :v_ba) {
				for(Corres cor : crs) {
					if(vba.id == cor.oldid) vba.newid = cor.newid;
				}
			}
			for(Virtual_BA vba : v_ba) {
				for(Integer inid : vba.inid) {
					for(Corres cor :crs) {
						if(inid == cor.oldid) vba.new_inid.add(cor.newid);
					}
				}
			}

			for(Virtual_BA vba : v_ba) {
				for(Integer outid : vba.outid) {
					for(Corres cor :crs) {
						if(outid == cor.oldid) vba.new_outid.add(cor.newid);
					}
				}
			}

			for (int i = 0; i < bank.size(); i++) {
				bank.get(i).Inid = v_ba.get(i).get_new_Inid();// <arraylist>i番目の銀行の入ってきている銀行のidリスト
				bank.get(i).Outid = v_ba.get(i).get_new_Outid();//出て行ってい金融機関のid
			}

			for(Institution b:bank) {
				if(b.Inid.contains(b.id)) System.out.println("Same index Inid!!");
				if(b.Outid.contains(b.id)) System.out.println("Same index Outid!!");
			}

			//In,Outid=0をdelete
			for (Institution b : bank) {
				Iterator<Integer> inid = b.Inid.iterator();
				while (inid.hasNext()) {
					Integer id = inid.next();
					if (id == 0)
						inid.remove();
				}
				Iterator<Integer> outid = b.Outid.iterator();
				while (outid.hasNext()) {
					Integer id = outid.next();
					if (id == 0)
						outid.remove();
				}
			}
			v_ba.clear();
			//v_ba.stream().forEach(s -> System.out.println(s.newid+" id : size"+(s.new_inid.size()+s.new_outid.size())));
			break;

		}
		//総次数の計算
		bank.stream().forEach(s->s.deg = s.Inid.size()+s.Outid.size());
		for(Institution b:bank) degsum += b.deg;
	}



	public void AdjustBS() {

		bank.stream().forEach(s -> s.setInitIBBS());

		for (Institution b : bank) {
			if (b.totalassets > b.totalinvest) {
				//liqAを均等に追加
				double dif = b.totalassets - b.totalinvest;
				b.addExAsset(asset, dif);
			}
			if (b.totalassets < b.totalinvest) {
				//liqAを均等に減少
				double dif = b.totalinvest - b.totalassets;
				b.cutExasset(asset, dif);
			}
		}

		//nの設定
		bank.stream().forEach(s -> {
			s.setInitIBBS();
			s.setNetWorth();
		});

	}

	public void IBtranx() {
		for (Institution b : bank) {
			for (int i = 0; i < b.Inid.size(); i++) {
				IBShort ibs = new IBShort(b.Inid.get(i), ibstock * b.cpbrin.get(i) / wdegsum);
				b.ibsborrow.add(ibs);
				b.ibsconnectedbank.add(b.Inid.get(i));
			}
			for (int i = 0; i < b.Outid.size(); i++) {
				IBShort ibs = new IBShort(b.Outid.get(i), ibstock * b.cpbrout.get(i) / wdegsum);
				b.ibsloan.add(ibs);
				if (!b.ibsconnectedbank.contains(b.Outid.get(i)))
					b.ibsconnectedbank.add(b.Outid.get(i));
			}
		}
		//ibsの決定
		for (Institution b : bank) {
			for (IBShort ib : b.ibsloan) {
				b.lb += ib.money;
			}
			for (IBShort ib : b.ibsborrow) {
				b.bb += ib.money;
			}
		}

	}

	public void IBdeg() {
		//IB取引額の重み
		for (Institution b : bank) {
			for (int i = 0; i < b.Inid.size(); i++) {
				for (Institution bb : bank) {
					if (b.Inid.get(i) == bb.id) {
						b.cpbrin.add(b.deg + bb.deg);
						wdegsum += b.deg + bb.deg;
					}
				}
			}
			for (int i = 0; i < b.Outid.size(); i++) {
				for (Institution bb : bank) {
					if (b.Outid.get(i) == bb.id) {
						b.cpbrout.add(b.deg + bb.deg);
						wdegsum += b.deg + bb.deg;
					}
				}
			}
		}
	}

	public void allocExasset() {
		// TODO 自動生成されたメソッド・スタブ

		//ランダムにassetのindexを割り当て
		for (Institution b : bank) {
			Collections.shuffle(asset);
			if (b.rank == "mega") {
				for (int i = 0; i < megala; i++) {
					AssetList as = new AssetList(asset.get(i).id, asset.get(i).group, asset.get(i).vol, b.liqA / megala,
							asset.get(i).ascor);
					b.Exasset.add(as);
					asset.get(i).setTotalAmount(b.liqA / megala);
					b.initassetnum = megala;
				}
			}
			if (b.rank == "medium") {
				for (int i = 0; i < mediumla; i++) {
					AssetList as = new AssetList(asset.get(i).id, asset.get(i).group, asset.get(i).vol,
							b.liqA / mediumla, asset.get(i).ascor);
					b.Exasset.add(as);
					asset.get(i).setTotalAmount(b.liqA / mediumla);
					b.initassetnum = mediumla;
				}
			}
			if (b.rank == "small") {
				for (int i = 0; i < smallla; i++) {
					AssetList as = new AssetList(asset.get(i).id, asset.get(i).group, asset.get(i).vol,
							b.liqA / smallla, asset.get(i).ascor);
					b.Exasset.add(as);
					asset.get(i).setTotalAmount(b.liqA / smallla);
					b.initassetnum = smallla;
				}
			}
		}
	}

	public ArrayList<Institution> getFinalBank() {
		return bank;
	}

	public int getDegSum() {
		// TODO 自動生成されたメソッド・スタブ
		return degsum;
	}

//////network comparing////////////////////////////////////////////////////////////////////////////////////////////////

	//create constructor cp network with same degree of BA
	public int degba;
	public boolean isCreate;
	public BuildComplete(ArrayList<Institution> bank, ArrayList<Asset> asset, double bscallrate,
			double theta, double randIn, double randOut, double a1, double a2, int la, int megala, int mediumla,
			int smallla, double megaCAR, double mediumCAR, double smallCAR, int degba) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.bank.addAll(bank);
		this.asset.addAll(asset);
		this.bscallrate = bscallrate;
		this.theta = theta;
		this.randIn = randIn;
		this.randOut = randOut;
		this.a1 = a1;
		this.a2 = a2;
		this.la = la;
		this.megala=megala;
		this.mediumla=mediumla;
		this.smallla=smallla;
		this.megaCAR=megaCAR;
		this.mediumCAR=mediumCAR;
		this.smallCAR=smallCAR;
		this.degba = degba;
		setInfo();
	}



	public boolean isStartCP() {

		//create network here
		createCP();

		//decide car ratio(rank)
		decideRank();

		//calc weightdegsum
		IBdeg();

		//call loan,moneyの決定
		IBtranx();

		//adjust BS
		AdjustBS();

		allocExasset();

		//setCARD
		bank.stream().forEach(s->s.CARD = -10000);

		return isCreate;
	}

	public void createCP() {
		NetworkCP1 CP = new NetworkCP1(bank.size(), a1, a2, randIn, randOut);
		isCreate = CP.createCP_sameDegBA(degba);
		for (int i = 0; i < bank.size(); i++) {
			bank.get(i).Inid = CP.getInid(i);// <arraylist>i番目の銀行の入ってきている銀行のidリスト
			bank.get(i).Outid = CP.getOutid(i);//出て行ってい金融機関のid
		}
		CP.virtualClear();
		bank.stream().forEach(s->s.deg = s.Inid.size()+s.Outid.size());

	}

	public void Virtual_Node_Clear() {
		// TODO 自動生成されたメソッド・スタブ
		v_ba.clear();
	}
}
