package logic;

import java.util.ArrayList;

import asset.Asset;

public class BuildExAsset {

	public int la,megala,mediumla,smallla,lag;
	public double vol;
	public ArrayList<Asset> ExAsset=new ArrayList<Asset>();
	public double ascor;
	public double fsr;

	public BuildExAsset(int la, int megala, int mediumla, int smallla, int lag,double vol,double ascor, double fsr) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.la=la;
		this.megala=megala;this.mediumla=mediumla;this.smallla=smallla;
		this.lag=lag;
		this.vol=vol;
		this.ascor=ascor;
		this.fsr = fsr;
	}

	public void isBuild() {
		// TODO 自動生成されたメソッド・スタブ
		//Assetリストの作成
		int count=0;
		int assetnum=(int)(la/lag);//1グループあたりのasset数
		int countg=assetnum;
		for(int i=1;i<=lag;i++) {
			for(int j=count;j<countg;j++) {
				Asset a=new Asset(j+1,i,vol,ascor, fsr);
				ExAsset.add(a);
			}
			count+=(int)(la/lag);
			countg+=assetnum;
		}



	}

	public ArrayList<Asset> getFinalAsset() {
		// TODO 自動生成されたメソッド・スタブ
		return ExAsset;
	}


}
