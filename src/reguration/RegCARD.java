package reguration;

import java.util.ArrayList;

import bankagent.Institution;

public class RegCARD {

	public RegCARD() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public void createregCARDrandom(ArrayList<Institution> bank, double mincard, double maxcard) {
		// TODO 自動生成されたメソッド・スタブ
		bank.stream().forEach(s->s.setCARD(mincard,maxcard));
	}

	public void createregCARDrange(ArrayList<Institution> bank, double mincard, double maxcard, double range) {
		// TODO 自動生成されたメソッド・スタブ
		bank.stream().filter(s-> s.id <= bank.size()*range)
					 .forEach(s-> s.setCARD(mincard,maxcard));
	}

}
