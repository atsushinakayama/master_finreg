package home;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;

//import com.sun.rowset.internal.Row;

import asset.Asset;
import bankagent.IBShort;
import bankagent.Institution;
import calculator.CalcDebtRank;
import calculator.Correlatin_Calculater;
import comparator.GetFirmKComparator;
import edu.uci.ics.jung.algorithms.scoring.PageRank;
import edu.uci.ics.jung.graph.DirectedOrderedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import firmagent.Enterprise;
import logic.BrProcess;
import logic.Build;
import logic.BuildComplete;
import logic.BuildExAsset;
import logic.BuildFirm;
import logic.BuildNWByThreshold;
import logic.Experiment;
import output.FinalOutPut;
import output.FinalOutPutShort;
import output.FinalOutputFirm;
import output.MiddleOutPut;
import output.MiddleOutPutFirm;
import output.OutPut;
import output.OutPutShort;
import reguration.RegCARD;
import shock.Shock;
import shock.ShockToBank;

public class Main {

	private static FinalOutPut fop;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//初期状態
		////金融機関
		int banknode = 100;
		double E0 = 200000;//初期asset金額
		double ibrate = 0.10;//IBloan割合
		double MegaCAR = 0.15, MediumCAR = 0.11, SmallCAR = 0.09;
		double a1 = 0.1, a2 = 0.5;//自己資本比率の決定(上位a%)
		double bscallrate = 0.2;//IBstock割合
		double div = 0.8;//bankdivident（配当など0.75）
		// I/B network
		String NW = "BA";//CP or BA or TD (Core-peripheral, Barabasi-albert, Threshold)
		String BankType = "Sym";

		// Threshold model conditions
		double theta = 3.0;//BAの閾値
		double theta1 = 4.0,theta2=5.0,theta3 = 2.0; //TD　ネットワーク接続　閾値　比較実験用

		// Core-Peripheral model conditions
		double randIn = 0.05, randOut = 0.05;//CP:randomconnect

		// BA model conditions
		int m0_1 = 5, m0_2 = 10, m0_3 = 15;//初期完全ネットワーク構築ノード数
		int m_1 = 5, m_2 = 10, m_3 = 15; //BA 追加ノード数　conditions

		//外部資産
		double la1 = 0.5, la2 = 0.7;//市場性資産保有割合(50%-70%)
		double larate = 0.6;//市場性資産保有割合固定型
		int la = 20, megala = 12, mediumla = 7, smallla = 5;//市場性資産種類数，各保有種類数
		int lag = 5;//市場性資産グループ数
		double vol = 0.0;


		//対称型金融機関
		double A0 = 150;
		double asyCAR = 0.10;

		//企業
		int firmnode = 1000;//初期企業数
		double k0 = 50;//初期asset金額

		//市場状況
		double r = 0.01;//市場金利
		double rr = 0.001;//金利係数
		double dr = 0.001;//預金金利
		double liqAr = 0.001;//市場性資産収益
		int tau = 4;
		double depoutrate = 0.000;//預金引き出し割合
		double divrate = 0.9;// 配当の支払い比率

		//実験条件
		int atp = 3;//試行回数
		int x = 0;
		int term = 400;//longterm to shortterm
		int expterm = 5;//実験開始からの期間
		int endterm = 400 + expterm;//終了
		int startshortterm = term;
		int cmiterator = 1000;//secondCM何回まわすか
		int shortterm = 6;//ショートターム(i month)
		int grossterm = shortterm * expterm;
		int creditterm = 1;//クレジット市場でのターム
		int startterm = 50;//

		double z = 0.04;//銀行自己資本比率破綻条件
		int fz = 1;//企業市場からの撤退条件
		double rz = 0.10;//自己資本維持比率⇒初期自己資本比率を維持
		int d = 100;//参入location係数
		double e = 0.1;//参入sensibity係数
		double brCAR = 0.04;//銀行破綻条件
		double shockflucrate = 0.5;
		double ascor = 0.2;//asset間の相関
		double cardef = 0.03;//card差分(*rand())
		int maxmainbank = 100;
		int minmainbank = 30;
		double maxloanrate = 0.2;

		//与えるショック
		//asset
		int shockterm = 6;//shortterm
		double dec = -0.03;//下落率
		double fixedshockrate = 0.01;
		String target = "MOST";//MOST,RANDOM

		//regulation
		//card random()
		double mincard = 0.04;
		double maxcard = 0.06;
		double mincard1 = 0.04;
		double maxcard1 = 0.08;
		//CARD range
		double pattern1 = 0;
		double pattern2 = 0.1;
		double pattern3 = 0.5;
		double pattern4 = 1;

		//レバレッジの設定
		double lfmin = 1.5, lfmax = 20.0, lbmin = 0.080, lbmax = 0.160;
		double val = 0.5, valb = 0.005;//間隔
		double h = 0.03;//forgetting processs
		double sm = 0.3;//割引率（0.3)
		double vi = 0.45;//strength係数

		//金融機関
		double xx = 0.2;//riskaversion係数
		double psy = 0.1;//riskaversion乗数
		double alpha = 0.08;//riskcoefficient係数（バーゼル規制による供給可能流動性）
		double myu = 0.05;//貸出金利係数
		double sita = 0.1;//貸出金利条数

		//企業
		//double fi=0.1,beta=0.75;//生産能力(0.1),生産能力係数
		int Candmax = 10;//取引先限界数

		//企業（限界生産型）
		double fi = 0.5, beta = 0.83;
		double g = 0.8;//変動費率
		double c = 1;//破産コスト係数
		double lamda = 1;//partnerchoice(intensityofchoice)
		int initialbanknumber = 1;//初期状態取引候補先

		double f = 0.075;//変動費 自己資本割合分(0.03)
		double stc = 0.1;//固定費
		double pi = 0.3;//実利益減少割合	(0.3)

		ArrayList<Institution> Bank = new ArrayList<Institution>();
		ArrayList<Enterprise> Firm = new ArrayList<Enterprise>();
		ArrayList<Asset> Asset = new ArrayList<Asset>();

		ArrayList<Institution> InitBank = (ArrayList<Institution>) Bank.clone();

		//OutPutリスト
		//Res
		ArrayList<OutPut> OutputRes = new ArrayList<OutPut>();
		for (int i = 1; i <= atp; i++) {
			OutPut op = new OutPut(i, term);
			OutputRes.add(op);
		}
		ArrayList<FinalOutPut> FinalOutputRes = new ArrayList<FinalOutPut>();
		FinalOutPut opres = new FinalOutPut(atp, term);
		FinalOutputRes.add(opres);

		//比較実験output①
		ArrayList<OutPut> Output = new ArrayList<OutPut>();
		for (int i = 1; i <= atp; i++) {
			OutPut op = new OutPut(i, term);
			Output.add(op);
		}

		ArrayList<OutPutShort> OutputShort = new ArrayList<OutPutShort>();
		for (int i = 1; i <= atp; i++) {
			OutPutShort ops = new OutPutShort(i, expterm, shortterm);
			OutputShort.add(ops);
		}
		ArrayList<FinalOutPutShort> FinalOutputShort = new ArrayList<FinalOutPutShort>();
		FinalOutPutShort op = new FinalOutPutShort(atp, expterm, shortterm);
		FinalOutputShort.add(op);

		//比較実験output②
		ArrayList<OutPut> Output1 = new ArrayList<OutPut>();
		for (int i = 1; i <= atp; i++) {
			OutPut op1 = new OutPut(i, expterm);
			Output1.add(op1);
		}

		ArrayList<OutPutShort> OutputShort1 = new ArrayList<OutPutShort>();
		for (int i = 1; i <= atp; i++) {
			OutPutShort ops = new OutPutShort(i, expterm, shortterm);
			OutputShort1.add(ops);
		}

		ArrayList<FinalOutPutShort> FinalOutputShort1 = new ArrayList<FinalOutPutShort>();
		FinalOutPutShort op1 = new FinalOutPutShort(atp, expterm, shortterm);
		FinalOutputShort1.add(op1);

		//output3
		ArrayList<OutPut> Output2 = new ArrayList<OutPut>();
		for (int i = 1; i <= atp; i++) {
			OutPut op2 = new OutPut(i, expterm);
			Output2.add(op2);
		}

		ArrayList<OutPutShort> OutputShort2 = new ArrayList<OutPutShort>();
		for (int i = 1; i <= atp; i++) {
			OutPutShort ops = new OutPutShort(i, expterm, shortterm);
			OutputShort2.add(ops);
		}

		ArrayList<FinalOutPutShort> FinalOutputShort2 = new ArrayList<FinalOutPutShort>();
		FinalOutPutShort op2 = new FinalOutPutShort(atp, expterm, shortterm);
		FinalOutputShort2.add(op2);

		//output4
		ArrayList<OutPut> Output3 = new ArrayList<OutPut>();
		for (int i = 1; i <= atp; i++) {
			OutPut op3 = new OutPut(i, expterm);
			Output3.add(op3);
		}

		ArrayList<OutPutShort> OutputShort3 = new ArrayList<OutPutShort>();
		for (int i = 1; i <= atp; i++) {
			OutPutShort ops = new OutPutShort(i, expterm, shortterm);
			OutputShort3.add(ops);
		}

		ArrayList<FinalOutPutShort> FinalOutputShort3 = new ArrayList<FinalOutPutShort>();
		FinalOutPutShort op3 = new FinalOutPutShort(atp, expterm, shortterm);
		FinalOutputShort3.add(op3);

		ArrayList<OutPutShort> OutputShort4 = new ArrayList<OutPutShort>();
		for (int i = 1; i <= atp; i++) {
			OutPutShort ops = new OutPutShort(i, expterm, shortterm);
			OutputShort4.add(ops);
		}

		ArrayList<FinalOutPutShort> FinalOutputShort4 = new ArrayList<FinalOutPutShort>();
		FinalOutPutShort op4 = new FinalOutPutShort(atp, expterm, shortterm);
		FinalOutputShort4.add(op4);

		ArrayList<OutPutShort> OutputShort5 = new ArrayList<OutPutShort>();
		for (int i = 1; i <= atp; i++) {
			OutPutShort ops = new OutPutShort(i, expterm, shortterm);
			OutputShort5.add(ops);
		}

		ArrayList<FinalOutPutShort> FinalOutputShort5 = new ArrayList<FinalOutPutShort>();
		FinalOutPutShort op5 = new FinalOutPutShort(atp, expterm, shortterm);
		FinalOutputShort5.add(op5);

		ArrayList<OutPutShort> OutputShort6 = new ArrayList<OutPutShort>();
		for (int i = 1; i <= atp; i++) {
			OutPutShort ops = new OutPutShort(i, expterm, shortterm);
			OutputShort6.add(ops);
		}

		ArrayList<FinalOutPutShort> FinalOutputShort6 = new ArrayList<FinalOutPutShort>();
		FinalOutPutShort op6 = new FinalOutPutShort(atp, expterm, shortterm);
		FinalOutputShort6.add(op6);

		ArrayList<OutPutShort> OutputShort7 = new ArrayList<OutPutShort>();
		for (int i = 1; i <= atp; i++) {
			OutPutShort ops = new OutPutShort(i, expterm, shortterm);
			OutputShort7.add(ops);
		}

		ArrayList<FinalOutPutShort> FinalOutputShort7 = new ArrayList<FinalOutPutShort>();
		FinalOutPutShort op7 = new FinalOutPutShort(atp, expterm, shortterm);
		FinalOutputShort7.add(op7);



		//middleoutput bank
		ArrayList<MiddleOutPut> MiddleOutput = new ArrayList<MiddleOutPut>();
		for (int i = 0; i < banknode; i++) {
			MiddleOutPut mop = new MiddleOutPut(i, atp);
			MiddleOutput.add(mop);
		}

		//Network比較用
		ArrayList<MiddleOutPut> MiddleOutput1= new ArrayList<MiddleOutPut>();
		for (int i = 0; i < banknode; i++) {
			MiddleOutPut mop = new MiddleOutPut(i, atp);
			MiddleOutput1.add(mop);
		}

		ArrayList<MiddleOutPut> MiddleOutput2= new ArrayList<MiddleOutPut>();
		for (int i = 0; i < banknode; i++) {
			MiddleOutPut mop = new MiddleOutPut(i, atp);
			MiddleOutput2.add(mop);
		}

		ArrayList<MiddleOutPut> MiddleOutput3= new ArrayList<MiddleOutPut>();
		for (int i = 0; i < banknode; i++) {
			MiddleOutPut mop = new MiddleOutPut(i, atp);
			MiddleOutput3.add(mop);
		}

		ArrayList<MiddleOutPut> MiddleOutput4= new ArrayList<MiddleOutPut>();
		for (int i = 0; i < banknode; i++) {
			MiddleOutPut mop = new MiddleOutPut(i, atp);
			MiddleOutput4.add(mop);
		}

		ArrayList<MiddleOutPut> MiddleOutput5= new ArrayList<MiddleOutPut>();
		for (int i = 0; i < banknode; i++) {
			MiddleOutPut mop = new MiddleOutPut(i, atp);
			MiddleOutput5.add(mop);
		}

		ArrayList<MiddleOutPut> MiddleOutput6= new ArrayList<MiddleOutPut>();
		for (int i = 0; i < banknode; i++) {
			MiddleOutPut mop = new MiddleOutPut(i, atp);
			MiddleOutput6.add(mop);
		}

		ArrayList<MiddleOutPut> MiddleOutput7= new ArrayList<MiddleOutPut>();
		for (int i = 0; i < banknode; i++) {
			MiddleOutPut mop = new MiddleOutPut(i, atp);
			MiddleOutput7.add(mop);
		}

		//middleoutput firm
		ArrayList<MiddleOutPutFirm> MiddleOutputFirm = new ArrayList<MiddleOutPutFirm>();
		for (int i = 0; i < firmnode; i++) {
			MiddleOutPutFirm mopf = new MiddleOutPutFirm(i, atp);
			MiddleOutputFirm.add(mopf);
		}

		//shock output
		ArrayList<ShockToBank> stb = new ArrayList<ShockToBank>();

		//相関の計算
		ArrayList<Correlation> cor = new ArrayList<Correlation>();
		for (int i = 0; i < atp; i++) {
			Correlation co = new Correlation(i, atp);
			cor.add(co);
		}

		//Network Coefficient
		ArrayList<NWCoefficient> nwcoef = new ArrayList<NWCoefficient>();
		for (int i=0;i<atp;i++) {
			NWCoefficient coef = new NWCoefficient(i,atp);
			nwcoef.add(coef);
		}

		//

		//create clone
		ArrayList<Institution> xBank = new ArrayList<Institution>();
		ArrayList<Institution> yBank = new ArrayList<Institution>();
		ArrayList<Institution> zBank = new ArrayList<Institution>();
		ArrayList<Institution> aBank = new ArrayList<Institution>();
		ArrayList<Institution> bBank = new ArrayList<Institution>();
		ArrayList<Institution> cBank = new ArrayList<Institution>();
		ArrayList<Institution> dBank = new ArrayList<Institution>();
		ArrayList<Enterprise> xFirm = new ArrayList<Enterprise>();
		ArrayList<Enterprise> yFirm = new ArrayList<Enterprise>();
		ArrayList<Enterprise> zFirm = new ArrayList<Enterprise>();
		ArrayList<Enterprise> wFirm = new ArrayList<Enterprise>();
		ArrayList<Enterprise> aFirm = new ArrayList<Enterprise>();
		ArrayList<Enterprise> bFirm = new ArrayList<Enterprise>();
		ArrayList<Enterprise> cFirm = new ArrayList<Enterprise>();
		ArrayList<Enterprise> dFirm = new ArrayList<Enterprise>();
		ArrayList<Asset> xAsset = new ArrayList<Asset>();
		ArrayList<Asset> yAsset = new ArrayList<Asset>();
		ArrayList<Asset> zAsset = new ArrayList<Asset>();
		ArrayList<Asset> wAsset = new ArrayList<Asset>();
		ArrayList<Asset> aAsset = new ArrayList<Asset>();
		ArrayList<Asset> bAsset = new ArrayList<Asset>();
		ArrayList<Asset> cAsset = new ArrayList<Asset>();
		ArrayList<Asset> dAsset = new ArrayList<Asset>();
		ArrayList<BrProcess> xVand = new ArrayList<BrProcess>();
		ArrayList<BrProcess> yVand = new ArrayList<BrProcess>();
		ArrayList<BrProcess> zVand = new ArrayList<BrProcess>();
		ArrayList<BrProcess> wVand = new ArrayList<BrProcess>();
		ArrayList<BrProcess> aVand = new ArrayList<BrProcess>();
		ArrayList<BrProcess> bVand = new ArrayList<BrProcess>();
		ArrayList<BrProcess> cVand = new ArrayList<BrProcess>();
		ArrayList<BrProcess> dVand = new ArrayList<BrProcess>();

		//start
		for (int y = 0; y < atp; y++) {
			System.out.println(y + "試行目");

			//市場性資産リスト作成
			BuildExAsset Exasset = new BuildExAsset(la, megala, mediumla, smallla, lag, vol, ascor, fixedshockrate);
			Exasset.isBuild();
			Asset.addAll(Exasset.getFinalAsset());

			//各エージェントバランスシート作成

			//bankバランスシート、初期ネットワーク構築
			Build BankBS = new Build(banknode, E0, MegaCAR, MediumCAR, SmallCAR, a1, a2, NW, BankType, A0, asyCAR,
					lbmin, lbmax, valb, vi, randIn, randOut, theta, ibrate, cardef);
			BankBS.isBuild();
			//BankBS.addExAsset(la1, la2);
			BankBS.addExAsset1(larate);
			//BankBS.allocExasset(Asset, la, megala, mediumla, smallla);//BankにAssetリストを渡す
			Bank.addAll(BankBS.getFinalBank());

			for (Institution b : Bank) {
				InitBank.add(b.clone());
			}

			//firmバランスシート作成（A(i0))
			BuildFirm FirmBS = new BuildFirm(firmnode, k0, initialbanknumber, banknode, f, lfmin, lfmax, val, vi,
					maxmainbank, minmainbank);
			FirmBS.isBuild();
			FirmBS.BankConnectDiversity(Bank);
			Firm.addAll(FirmBS.getFinalFirm());
			//Firm.addAll(FirmBS1.getFinalFirm());

			//破綻処理機関リスト
			ArrayList<BrProcess> Vand = new ArrayList<BrProcess>();
			BrProcess vand = new BrProcess(1);
			Vand.add(vand);

			//初期状態出力(予備)(BA,CPチェック)
			//showInitExcel(Bank,Firm);}

			//初期銀行状態の格納
			InitBankOutput(Output, InitBank, y);

			//実験用クラス
			//create bank-firm network
			Experiment ExpRes = new Experiment(Bank, Firm, Vand, banknode, firmnode, r, tau, term, z, xx, psy,
					alpha, myu, sita, fi, g, c, lamda, initialbanknumber, d, e, k0, dr, startterm, div, liqAr,
					creditterm, bscallrate, rz, beta, h, sm, lfmin, lfmax, val, pi, vi, Candmax, rr, shortterm,
					depoutrate, vol, Asset, brCAR, shockflucrate, cmiterator, fz, stc, f, maxmainbank, startshortterm,
					MiddleOutput, MiddleOutputFirm, minmainbank, cor, y, "Build");
			ExpRes.isStart();
			if (ExpRes.getBrCount() > 0) {
				System.out.println("やりなおし");
				y--;
			} else {
				Bank.stream().forEach(s -> s.CARD = -10000);//CAR<0の銀行をrolloverするため

// Threshold model//
//////////////////銀行バランスシートの完成(network固定)/////////////////////////////////
				/*
				//ExpRes.createBankBSComp(la, megala, mediumla, smallla, NW, theta, randIn, randOut,a1,a2);
				BuildComplete BankBSCom = new BuildComplete(Bank,Asset,bscallrate,NW,theta,randIn,randOut,a1,a2,la, megala, mediumla, smallla, MegaCAR,MediumCAR,SmallCAR);
				BankBSCom.isStart();


				//middleOutPut
				ExpRes.createMiddleOutPut();
				//ExpRes.OutPutResult(OutputRes, y);
				ExpRes.clearList();

				//create Clone(Bank)
				for (Institution b : Bank) {
					xBank.add(b.clone());
					yBank.add(b.clone());
					zBank.add(b.clone());
					wBank.add(b.clone());
				}
				*/
/////////////////// network comparing here (to same degree) ////////////////////////////////////////
				/*
				// create clone (CP network)
				for(Institution b:Bank) {
					yBank.add(b.clone());
				}

				int degba;//(BAの次数に合わせる)
				//BA
				BuildComplete bc = new BuildComplete(Bank,Asset,bscallrate,"BA",theta,randIn,randOut,a1,a2,la,megala,mediumla,smallla,MegaCAR,MediumCAR,SmallCAR);
				bc.isStart();
				degba = bc.getDegSum();
				//create clone
				for(Institution b:Bank) xBank.add(b.clone());

				// CP
				boolean isCreate;
				BuildComplete bcCP = new BuildComplete(yBank,Asset,bscallrate,theta,randIn,randOut,a1,a2,la,megala,mediumla,smallla,MegaCAR,MediumCAR,SmallCAR,degba);
				isCreate = bcCP.isStartCP();
				if(isCreate == false) {
					AgentClear(Bank, Firm, Vand, InitBank, xBank, xFirm, xVand, Asset, xAsset, yBank, yFirm, yAsset, yVand,zBank, zFirm, zAsset, zVand);
					y--;
					System.out.println("やり直し");
					break;
				}else {

				// create clone
				for(Institution b:yBank) zBank.add(b.clone());

				//create middle output
				OutputMiddle(Bank,MiddleOutput);
				OutputMiddle(yBank,MiddleOutput1);

				// NW analysis and output
				OutputNW(Bank,"BA");
				OutputNW(yBank,"CP");
				*/
//////////////// network comparing degree theta here (TD) //////////////////////////////////////////////
				/*
				for (Asset as : Asset) {
					xAsset.add(as.clone());
					yAsset.add(as.clone());
					zAsset.add(as.clone());
					//wAsset.add(as.clone());
					aAsset.add(as.clone());
					bAsset.add(as.clone());
					cAsset.add(as.clone());
					dAsset.add(as.clone());
				}

				// degree variables
				int degba,degba1,degba2,degba3;

				BuildComplete bc = new BuildComplete(Bank,Asset,bscallrate,"BA",theta,randIn,randOut,a1,a2,la,megala,mediumla,smallla,MegaCAR,MediumCAR,SmallCAR);
				bc.isStart();
				degba = bc.getDegSum();

				for(Institution b:Bank) {
					xBank.add(b.clone());
					yBank.add(b.clone());
					zBank.add(b.clone());
					aBank.add(b.clone());
					bBank.add(b.clone());
					cBank.add(b.clone());
					//dBank.add(b.clone());
				}

				//　I/B以外の部分を同じ条件で構築
				BuildNWByThreshold bt = new BuildNWByThreshold(xBank,xAsset,theta1,bscallrate,Bank);
				bt.isStart("BA");
				degba1 = bt.getDegSum();
				BuildNWByThreshold bt1 = new BuildNWByThreshold(yBank,yAsset,theta2,bscallrate,Bank);
				bt1.isStart("BA");
				degba2 = bt1.getDegSum();
				*/
				/*
				BuildNWByThreshold bt2 = new BuildNWByThreshold(zBank,Asset,theta3,bscallrate);
				bt2.isStart("BA");
				degba3 = bt2.getDegSum();
				*/
				/*
				// create cp network with same conditions
				BuildNWByThreshold ct = new BuildNWByThreshold(aBank,aAsset,bscallrate,randIn,randOut,degba,a1,a2,Bank);
				ct.isStart("CP");
				BuildNWByThreshold ct1 = new BuildNWByThreshold(bBank,bAsset,bscallrate,randIn,randOut,degba1,a1,a2,Bank);
				ct1.isStart("CP");
				BuildNWByThreshold ct2 = new BuildNWByThreshold(cBank,cAsset,bscallrate,randIn,randOut,degba2,a1,a2,Bank);
				ct2.isStart("CP");

				OutputMiddle(Bank,MiddleOutput);
				OutputMiddle(xBank,MiddleOutput1);
				OutputMiddle(yBank,MiddleOutput2);
				//OutputMiddle(zBank,MiddleOutput3);
				OutputMiddle(aBank,MiddleOutput4);
				OutputMiddle(bBank,MiddleOutput5);
				OutputMiddle(cBank,MiddleOutput6);
				*/


///////////// Same degree amoung TD-CP Network include financial regulation////////////////////////////////////////////////////////////////////////////////////

				/*
				 * 金融規制のパターンを変えた場合
				 */
				/*
				for(Asset as : Asset) {
					aAsset.add(as.clone());
				}

				// degree variables
				int degba,degba1,degba2,degba3;

				//BA network
				BuildComplete bc = new BuildComplete(Bank,Asset,bscallrate,NW,theta1,randIn,randOut,a1,a2,la,megala,mediumla,smallla,MegaCAR,MediumCAR,SmallCAR);
				bc.isStart();
				degba = bc.getDegSum();

				for(Institution b:Bank) {
					xBank.add(b.clone());
					yBank.add(b.clone());
					zBank.add(b.clone());
					aBank.add(b.clone());
					//bBank.add(b.clone());
					//cBank.add(b.clone());
					//dBank.add(b.clone());
				}

				for (Asset as : Asset) {
					xAsset.add(as.clone());
					yAsset.add(as.clone());
					zAsset.add(as.clone());
				}
				//Core-peripheral
				BuildNWByThreshold ct = new BuildNWByThreshold(aBank,aAsset,bscallrate,randIn,randOut,degba,a1,a2,Bank);
				ct.isStart("CP");

				for(Institution b:aBank) {
					bBank.add(b.clone());
					cBank.add(b.clone());
					dBank.add(b.clone());
				}

				for(Asset as :aAsset) {
					bAsset.add(as.clone());
					cAsset.add(as.clone());
					dAsset.add(as.clone());
				}

				OutputMiddle(Bank,MiddleOutput);
				OutputMiddle(xBank,MiddleOutput1);
				OutputMiddle(yBank,MiddleOutput2);
				OutputMiddle(zBank,MiddleOutput3);
				OutputMiddle(aBank,MiddleOutput4);
				OutputMiddle(bBank,MiddleOutput5);
				OutputMiddle(cBank,MiddleOutput6);
				OutputMiddle(dBank,MiddleOutput7);
				*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// BA model ////////////////////////////////////////////////////////////////////////////////////////////////////////

	/////////////// network comparing degree theta here (BA) //////////////////////////////////////////////
					/*
					for (Asset as : Asset) {
						xAsset.add(as.clone());
						yAsset.add(as.clone());
						zAsset.add(as.clone());
						//wAsset.add(as.clone());
						aAsset.add(as.clone());
						bAsset.add(as.clone());
						cAsset.add(as.clone());
						dAsset.add(as.clone());
					}

					// degree variables
					int degba,degba1,degba2;

					BuildComplete bc = new BuildComplete(Bank,Asset,bscallrate,"BA",m0_1,m_1,randIn,randOut,a1,a2,la,megala,mediumla,smallla,MegaCAR,MediumCAR,SmallCAR);
					bc.isStart();
					degba = bc.getDegSum();

					for(Institution b:Bank) {
						xBank.add(b.clone());
						yBank.add(b.clone());
						zBank.add(b.clone());
						aBank.add(b.clone());
						bBank.add(b.clone());
						cBank.add(b.clone());
						//dBank.add(b.clone());
					}

					//　I/B以外の部分を同じ条件で構築
					BuildNWByThreshold bt = new BuildNWByThreshold(xBank,xAsset,m0_2,m_2,bscallrate,Bank);
					bt.isStart("BA");
					degba1 = bt.getDegSum();
					BuildNWByThreshold bt1 = new BuildNWByThreshold(yBank,yAsset,m0_3,m_3,bscallrate,Bank);
					bt1.isStart("BA");
					degba2 = bt1.getDegSum();
					*/

					/*
					BuildNWByThreshold bt2 = new BuildNWByThreshold(zBank,Asset,theta3,bscallrate);
					bt2.isStart("BA");
					degba3 = bt2.getDegSum();
					*/
					/*
					// create cp network with same conditions
					BuildNWByThreshold ct = new BuildNWByThreshold(aBank,aAsset,bscallrate,randIn,randOut,degba,a1,a2,Bank);
					ct.isStart("CP");
					BuildNWByThreshold ct1 = new BuildNWByThreshold(bBank,bAsset,bscallrate,randIn,randOut,degba1,a1,a2,Bank);
					ct1.isStart("CP");
					BuildNWByThreshold ct2 = new BuildNWByThreshold(cBank,cAsset,bscallrate,randIn,randOut,degba2,a1,a2,Bank);
					ct2.isStart("CP");

					OutputMiddle(Bank,MiddleOutput);
					OutputMiddle(xBank,MiddleOutput1);
					OutputMiddle(yBank,MiddleOutput2);
					//OutputMiddle(zBank,MiddleOutput3);
					OutputMiddle(aBank,MiddleOutput4);
					OutputMiddle(bBank,MiddleOutput5);
					OutputMiddle(cBank,MiddleOutput6);
					*/

	/////////////// Same_deg Financial regulation //////////////////////////////////////////////

				/*
				 * 金融規制のパターンを変えた場合
				 */

				for(Asset as : Asset) {
					aAsset.add(as.clone());
				}

				// degree variables
				int degba,degba1,degba2,degba3;

				//BA network
				BuildComplete bc = new BuildComplete(Bank,Asset,bscallrate,NW,m0_1,m_1,randIn,randOut,a1,a2,la,megala,mediumla,smallla,MegaCAR,MediumCAR,SmallCAR);
				bc.isStart();
				degba = bc.getDegSum();

				for(Institution b:Bank) {
					xBank.add(b.clone());
					yBank.add(b.clone());
					zBank.add(b.clone());
					aBank.add(b.clone());
					//bBank.add(b.clone());
					//cBank.add(b.clone());
					//dBank.add(b.clone());
				}

				for (Asset as : Asset) {
					xAsset.add(as.clone());
					yAsset.add(as.clone());
					zAsset.add(as.clone());
				}
				//Core-peripheral
				BuildNWByThreshold ct = new BuildNWByThreshold(aBank,aAsset,bscallrate,randIn,randOut,degba,a1,a2,Bank);
				ct.isStart("CP");

				for(Institution b:aBank) {
					bBank.add(b.clone());
					cBank.add(b.clone());
					dBank.add(b.clone());
				}

				for(Asset as :aAsset) {
					bAsset.add(as.clone());
					cAsset.add(as.clone());
					dAsset.add(as.clone());
				}

				OutputMiddle(Bank,MiddleOutput);
				OutputMiddle(xBank,MiddleOutput1);
				OutputMiddle(yBank,MiddleOutput2);
				OutputMiddle(zBank,MiddleOutput3);
				OutputMiddle(aBank,MiddleOutput4);
				OutputMiddle(bBank,MiddleOutput5);
				OutputMiddle(cBank,MiddleOutput6);
				OutputMiddle(dBank,MiddleOutput7);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				//finalresfirm out
				Firm.clear();
				Firm.addAll(ExpRes.getFinalResFirm());

				//sort agent
				Collections.sort(Firm, new GetFirmKComparator());

				//create clone(other)
				for (Enterprise ff : Firm) {
					xFirm.add(ff.clone());
					yFirm.add(ff.clone());
					zFirm.add(ff.clone());
					wFirm.add(ff.clone());
					aFirm.add(ff.clone());
					bFirm.add(ff.clone());
					cFirm.add(ff.clone());
					dFirm.add(ff.clone());
				}

				for (BrProcess v : Vand) {
					xVand.add(v.clone());
					yVand.add(v.clone());
					zVand.add(v.clone());
					wVand.add(v.clone());
					aVand.add(v.clone());
					bVand.add(v.clone());
					cVand.add(v.clone());
					dVand.add(v.clone());
				}

				//set object-object clone
				xBank.stream().forEach(s -> s.setCloneList());
				xFirm.stream().forEach(s -> s.setCloneList());
				yBank.stream().forEach(s -> s.setCloneList1());
				yFirm.stream().forEach(s -> s.setCloneList1());
				zBank.stream().forEach(s -> s.setCloneList2());
				zFirm.stream().forEach(s -> s.setCloneList2());

				aBank.stream().forEach(s -> s.setCloneList3());
				aFirm.stream().forEach(s -> s.setCloneList3());
				bBank.stream().forEach(s -> s.setCloneList4());
				bFirm.stream().forEach(s -> s.setCloneList4());
				cBank.stream().forEach(s -> s.setCloneList5());
				cFirm.stream().forEach(s -> s.setCloneList5());
				dBank.stream().forEach(s -> s.setCloneList6());
				dFirm.stream().forEach(s -> s.setCloneList6());

				/////shock create
				Shock shock = new Shock(shockterm);
				//asset
				/*
				shock.createAssetShock(Asset, dec, target);
				shock.createAssetShock(xAsset, dec, target);
				shock.createAssetShock(yAsset, dec, target);
				//shock.createAssetShock(zAsset, dec, target);
				shock.createAssetShock(aAsset, dec, target);
				shock.createAssetShock(bAsset, dec, target);
				shock.createAssetShock(cAsset, dec, target);
				*/
				// same asset id shock
				int assetid;
				assetid = shock.createAssetShock1(Asset, dec, target);
				shock.assignAssetShock(xAsset, dec, assetid);
				shock.assignAssetShock(yAsset, dec, assetid);
				shock.assignAssetShock(zAsset, dec, assetid);
				shock.assignAssetShock(aAsset, dec, assetid);
				shock.assignAssetShock(bAsset, dec, assetid);
				shock.assignAssetShock(cAsset, dec, assetid);
				shock.assignAssetShock(dAsset, dec, assetid);

				//firm
				//資産規模が大きい企業の破綻
				/*
				shock.createLargestFirmShock(Firm,Bank);
				shock.createLargestFirmShock(xFirm,xBank);
				shock.createLargestFirmShock(yFirm,yBank);
				shock.createLargestFirmShock(zFirm,zBank);
				shock.createLargestFirmShock(aFirm,aBank);
				shock.createLargestFirmShock(bFirm,bBank);
				shock.createLargestFirmShock(cFirm,cBank);
				shock.createLargestFirmShock(dFirm,dBank);
				*/
				//規模真ん中の企業の破綻
				shock.createMiddleFirmShock(Firm,Bank);
				shock.createMiddleFirmShock(xFirm,xBank);
				shock.createMiddleFirmShock(yFirm,yBank);
				shock.createMiddleFirmShock(zFirm,zBank);
				shock.createMiddleFirmShock(aFirm,aBank);
				shock.createMiddleFirmShock(bFirm,bBank);
				shock.createMiddleFirmShock(cFirm,cBank);
				shock.createMiddleFirmShock(dFirm,dBank);

				//bank
				//shock.createLargestBankShock(xBank);
				// shock output
				stb.addAll(shock.getShocktobanklist());

				///regulation create
				RegCARD regCARD = new RegCARD();
				//exp(SSI2019)　比較対象 CARD :0.04~0.06,0.06~0.08
				/*
				regCARD.createregCARDrandom(xBank, mincard, maxcard);
				regCARD.createregCARDrandom(zBank, mincard, maxcard);
				*/
				//CARD 規制範囲の設定
				/*
				regCARD.createregCARDrange(Bank,mincard, maxcard, pattern1);
				regCARD.createregCARDrange(xBank,mincard, maxcard, pattern2);
				regCARD.createregCARDrange(yBank,mincard, maxcard, pattern3);
				regCARD.createregCARDrange(zBank,mincard, maxcard, pattern4);
				*/
				//CARD 規制水準範囲の設定(規制範囲は固定)

				regCARD.createregCARDrange(Bank,  mincard, maxcard, pattern1);
				regCARD.createregCARDrange(xBank, mincard, maxcard, pattern2);
				regCARD.createregCARDrange(yBank, mincard, maxcard, pattern3);
				regCARD.createregCARDrange(zBank, mincard, maxcard, pattern4);
				regCARD.createregCARDrange(aBank, mincard, maxcard, pattern1);
				regCARD.createregCARDrange(bBank, mincard, maxcard, pattern2);
				regCARD.createregCARDrange(cBank, mincard, maxcard, pattern3);
				regCARD.createregCARDrange(dBank, mincard, maxcard, pattern4);


				//実験スタート
				//pattern1
				Experiment Exp = new Experiment(Bank, Firm, Vand, banknode, firmnode, r, tau, expterm, z, xx, psy,
						alpha, myu, sita, fi, g, c, lamda, initialbanknumber, d, e, k0, dr, startterm, div, liqAr,
						creditterm, bscallrate, rz, beta, h, sm, lfmin, lfmax, val, pi, vi, Candmax, rr, shortterm,
						depoutrate, vol, Asset, brCAR, shockflucrate, cmiterator, fz, stc, f, maxmainbank,
						startshortterm,
						MiddleOutput, MiddleOutputFirm, minmainbank, cor, y, "Start");
				Exp.isStart();
				//Exp.OutPutResult(Output, y);
				Exp.OutPutShortResult(OutputShort, y);
				Exp.clearList();

				//pattern2
				xBank.stream().forEach(s -> s.addCloneList());
				xFirm.stream().forEach(s -> s.addCloneList());
				System.out.println(
						"-----------------------比較実験1---------------------------------------------------------");
				Experiment Exp1 = new Experiment(xBank, xFirm, xVand, banknode, firmnode, r, tau, expterm, z, xx, psy,
						alpha, myu, sita, fi, g, c, lamda, initialbanknumber, d, e, k0, dr, startterm, div, liqAr,
						creditterm, bscallrate, rz, beta, h, sm, lfmin, lfmax, val, pi, vi, Candmax, rr, shortterm,
						depoutrate, vol, xAsset, brCAR, shockflucrate, cmiterator, fz, stc, f, maxmainbank,
						startshortterm,
						MiddleOutput, MiddleOutputFirm, minmainbank, cor, y, "Start");
				Exp1.isStart();
				//Exp1.OutPutResult(Output1, y);
				Exp1.OutPutShortResult(OutputShort1, y);
				Exp1.clearList();

				//pattern3
				yBank.stream().forEach(s -> s.addCloneList1());
				yFirm.stream().forEach(s -> s.addCloneList1());
				System.out.println(
						"-----------------------比較実験2---------------------------------------------------------");
				Experiment Exp2 = new Experiment(yBank, yFirm, yVand, banknode, firmnode, r, tau, expterm, z, xx, psy,
						alpha, myu, sita, fi, g, c, lamda, initialbanknumber, d, e, k0, dr, startterm, div, liqAr,
						creditterm, bscallrate, rz, beta, h, sm, lfmin, lfmax, val, pi, vi, Candmax, rr, shortterm,
						depoutrate, vol, yAsset, brCAR, shockflucrate, cmiterator, fz, stc, f, maxmainbank,
						startshortterm,
						MiddleOutput, MiddleOutputFirm, minmainbank, cor, y, "Start");
				Exp2.isStart();
				//Exp2.OutPutResult(Output2, y);
				Exp2.OutPutShortResult(OutputShort2, y);
				Exp2.clearList();


				//pattern4
				zBank.stream().forEach(s -> s.addCloneList2());
				zFirm.stream().forEach(s -> s.addCloneList2());
				System.out.println(
						"-----------------------比較実験3---------------------------------------------------------");
				Experiment Exp3 = new Experiment(zBank, zFirm, zVand, banknode, firmnode, r, tau, expterm, z, xx, psy,
						alpha, myu, sita, fi, g, c, lamda, initialbanknumber, d, e, k0, dr, startterm, div, liqAr,
						creditterm, bscallrate, rz, beta, h, sm, lfmin, lfmax, val, pi, vi, Candmax, rr, shortterm,
						depoutrate, vol, zAsset, brCAR, shockflucrate, cmiterator, fz, stc, f, maxmainbank,
						startshortterm,
						MiddleOutput, MiddleOutputFirm, minmainbank, cor, y, "Start");
				Exp3.isStart();
				//Exp2.OutPutResult(Output2, y);
				Exp3.OutPutShortResult(OutputShort3, y);
				Exp3.clearList();


				// NW comparing CP

				aBank.stream().forEach(s -> s.addCloneList3());
				aFirm.stream().forEach(s -> s.addCloneList3());
				System.out.println(
						"-----------------------比較実験4---------------------------------------------------------");
				Experiment Exp4 = new Experiment(aBank, aFirm, aVand, banknode, firmnode, r, tau, expterm, z, xx, psy,
						alpha, myu, sita, fi, g, c, lamda, initialbanknumber, d, e, k0, dr, startterm, div, liqAr,
						creditterm, bscallrate, rz, beta, h, sm, lfmin, lfmax, val, pi, vi, Candmax, rr, shortterm,
						depoutrate, vol, aAsset, brCAR, shockflucrate, cmiterator, fz, stc, f, maxmainbank,
						startshortterm,
						MiddleOutput, MiddleOutputFirm, minmainbank, cor, y, "Start");
				Exp4.isStart();
				//Exp2.OutPutResult(Output2, y);
				Exp4.OutPutShortResult(OutputShort4, y);
				Exp4.clearList();

				bBank.stream().forEach(s -> s.addCloneList4());
				bFirm.stream().forEach(s -> s.addCloneList4());
				System.out.println(
						"-----------------------比較実験5---------------------------------------------------------");
				Experiment Exp5 = new Experiment(bBank, bFirm, bVand, banknode, firmnode, r, tau, expterm, z, xx, psy,
						alpha, myu, sita, fi, g, c, lamda, initialbanknumber, d, e, k0, dr, startterm, div, liqAr,
						creditterm, bscallrate, rz, beta, h, sm, lfmin, lfmax, val, pi, vi, Candmax, rr, shortterm,
						depoutrate, vol, bAsset, brCAR, shockflucrate, cmiterator, fz, stc, f, maxmainbank,
						startshortterm,
						MiddleOutput, MiddleOutputFirm, minmainbank, cor, y, "Start");
				Exp5.isStart();
				//Exp2.OutPutResult(Output2, y);
				Exp5.OutPutShortResult(OutputShort5, y);
				Exp5.clearList();

				cBank.stream().forEach(s -> s.addCloneList5());
				cFirm.stream().forEach(s -> s.addCloneList5());
				System.out.println(
						"-----------------------比較実験6---------------------------------------------------------");
				Experiment Exp6 = new Experiment(cBank, cFirm, cVand, banknode, firmnode, r, tau, expterm, z, xx, psy,
						alpha, myu, sita, fi, g, c, lamda, initialbanknumber, d, e, k0, dr, startterm, div, liqAr,
						creditterm, bscallrate, rz, beta, h, sm, lfmin, lfmax, val, pi, vi, Candmax, rr, shortterm,
						depoutrate, vol, cAsset, brCAR, shockflucrate, cmiterator, fz, stc, f, maxmainbank,
						startshortterm,
						MiddleOutput, MiddleOutputFirm, minmainbank, cor, y, "Start");
				Exp6.isStart();
				//Exp2.OutPutResult(Output2, y);
				Exp6.OutPutShortResult(OutputShort6, y);
				Exp6.clearList();


				dBank.stream().forEach(s -> s.addCloneList6());
				dFirm.stream().forEach(s -> s.addCloneList6());
				System.out.println(
						"-----------------------比較実験7---------------------------------------------------------");
				Experiment Exp7 = new Experiment(dBank, dFirm, dVand, banknode, firmnode, r, tau, expterm, z, xx, psy,
						alpha, myu, sita, fi, g, c, lamda, initialbanknumber, d, e, k0, dr, startterm, div, liqAr,
						creditterm, bscallrate, rz, beta, h, sm, lfmin, lfmax, val, pi, vi, Candmax, rr, shortterm,
						depoutrate, vol, dAsset, brCAR, shockflucrate, cmiterator, fz, stc, f, maxmainbank,
						startshortterm,
						MiddleOutput, MiddleOutputFirm, minmainbank, cor, y, "Start");
				Exp7.isStart();
				//Exp2.OutPutResult(Output2, y);
				Exp7.OutPutShortResult(OutputShort7, y);
				Exp7.clearList();


				//object clear to build
				bc.Virtual_Node_Clear();


			}

			//showExcel(Exp1, Bank);
			//showAgent(Exp1, Firm);
			/*
			Bank.stream().forEach(s->System.out.println(s.CAR));
			System.out.println("----------");
			xBank.stream().forEach(s->System.out.println(s.CAR));
			*/

			AgentClear(Bank, Firm, Vand, InitBank, xBank, xFirm, xVand, Asset, xAsset, yBank, yFirm, yAsset, yVand,zBank, zFirm, zAsset, zVand);
			//agent clear
			aBank.clear();bBank.clear();cBank.clear();dBank.clear();
			aFirm.clear();bFirm.clear();cFirm.clear();dFirm.clear();
			aAsset.clear();bAsset.clear();cAsset.clear();dAsset.clear();
			aVand.clear();bVand.clear();cVand.clear();dVand.clear();


		}



		//ResultAdjustRes(FinalOutputRes, OutputRes, term, atp, firmnode);//平均，(標準偏差)
		//shortterm finaloutput adjust
		ResultAdjustExp1(FinalOutputShort, OutputShort);
		ResultAdjustExp1(FinalOutputShort1, OutputShort1);
		ResultAdjustExp1(FinalOutputShort2, OutputShort2);
		ResultAdjustExp1(FinalOutputShort3, OutputShort3);
		ResultAdjustExp1(FinalOutputShort4, OutputShort4);
		ResultAdjustExp1(FinalOutputShort5, OutputShort5);
		ResultAdjustExp1(FinalOutputShort6, OutputShort6);
		ResultAdjustExp1(FinalOutputShort7, OutputShort7);


		OutPutConditions(banknode, firmnode, r, tau, term, z, xx, psy,
				alpha, myu, sita, fi, g, c, lamda, initialbanknumber, d, e, k0, dr, startterm, div, liqAr, creditterm,
				bscallrate, rz, beta, h, sm, lfmin, lfmax, val, pi, vi, Candmax, rr, E0, f, a1, a2, la,
				megala, mediumla, smallla, theta, randIn, randOut);

		OutPutMiddleResult(MiddleOutput, MiddleOutputFirm, cor);
		//network 比較用

		OutPutMiddleResult(MiddleOutput,"BA(PT1)");
		OutPutMiddleResult(MiddleOutput1,"BA(PT2)");
		OutPutMiddleResult(MiddleOutput2,"BA(PT3)");
		OutPutMiddleResult(MiddleOutput3,"BA(PT4)");
		OutPutMiddleResult(MiddleOutput4,"CP(PT1)");
		OutPutMiddleResult(MiddleOutput5,"CP(PT2)");
		OutPutMiddleResult(MiddleOutput6,"CP(PT3)");
		OutPutMiddleResult(MiddleOutput7,"CP(PT4)");
		//OutPutResResult(FinalOutputRes, term, atp, firmnode, banknode);
		OutPutShock(stb,grossterm,atp);
		OutPutShortResult(FinalOutputShort, grossterm, expterm, atp, OutputShort,"");
		OutPutShortResult(FinalOutputShort1, grossterm, expterm, atp, OutputShort1, "1");
		OutPutShortResult(FinalOutputShort2, grossterm, expterm, atp, OutputShort2, "2");
		OutPutShortResult(FinalOutputShort3, grossterm, expterm, atp, OutputShort3, "3");
		OutPutShortResult(FinalOutputShort4, grossterm, expterm, atp, OutputShort4, "4");
		OutPutShortResult(FinalOutputShort5, grossterm, expterm, atp, OutputShort5, "5");
		OutPutShortResult(FinalOutputShort6, grossterm, expterm, atp, OutputShort6, "6");
		OutPutShortResult(FinalOutputShort7, grossterm, expterm, atp, OutputShort7, "7");

		//python analysis用 output
		OutPutShortforPython(FinalOutputShort,OutputShort,grossterm,expterm,atp,"BA","pt1");
		OutPutShortforPython(FinalOutputShort1,OutputShort1,grossterm,expterm,atp,"BA","pt2");
		OutPutShortforPython(FinalOutputShort2,OutputShort2,grossterm,expterm,atp,"BA","pt3");
		OutPutShortforPython(FinalOutputShort3,OutputShort3,grossterm,expterm,atp,"BA","pt4");
		OutPutShortforPython(FinalOutputShort4,OutputShort4,grossterm,expterm,atp,"CP","pt1");
		OutPutShortforPython(FinalOutputShort5,OutputShort5,grossterm,expterm,atp,"CP","pt2");
		OutPutShortforPython(FinalOutputShort6,OutputShort6,grossterm,expterm,atp,"CP","pt3");
		OutPutShortforPython(FinalOutputShort7,OutputShort7,grossterm,expterm,atp,"CP","pt4");

		//OutPutResultInit(FinalOutput, endterm, atp, firmnode, banknode);

		System.out.println("--------------------------------Experiment End------------------------------------");

	}

	private static void OutPutShortforPython(ArrayList<FinalOutPutShort> fops,
			ArrayList<OutPutShort> ops, int grossterm, int expterm, int atp, String nw, String pt) {
		// TODO 自動生成されたメソッド・スタブ
		Workbook book = null;
		FileOutputStream outExcelFile = null;
		String outputPath = "/Users/nakayamaatsushi/Desktop/WorkspaceJava/master_FinReg/py_Histgram/Results/";
		String fileName = "workbook"+nw+pt+".xls";
		try {
			book = new HSSFWorkbook();

			FileOutputStream fileOut = new FileOutputStream(outputPath+fileName);
			String safename = WorkbookUtil.createSafeSheetName("cascade_size");
			String safename1 = WorkbookUtil.createSafeSheetName("total_loss");
			String safename2 = WorkbookUtil.createSafeSheetName("cascade_size_by_size");
			String safename3 = WorkbookUtil.createSafeSheetName("FB_DR_Mega");
			String safename4 = WorkbookUtil.createSafeSheetName("FB_DR_Medium");
			String safename5 = WorkbookUtil.createSafeSheetName("FB_DR_Small");
			Sheet sheet = book.createSheet(safename);
			Sheet sheet1 = book.createSheet(safename1);
			Sheet sheet2 = book.createSheet(safename2);
			Sheet sheet3 = book.createSheet(safename3);
			Sheet sheet4 = book.createSheet(safename4);
			Sheet sheet5 = book.createSheet(safename5);
			CreationHelper createHelper = book.getCreationHelper();

			//残存数 and 規模ごと破綻数
			int counter = 0;
			for(OutPutShort o:ops) {
				Row row = sheet.createRow((short)counter);
				Row row_by_size = sheet2.createRow((short)counter);
				//全体
				row.createCell(0).setCellValue(100-o.AliveBankNum.get(grossterm-1));


				//規模ごと
				row_by_size.createCell(0).setCellValue(o.failed_mega);
				row_by_size.createCell(1).setCellValue(o.failed_medium);
				row_by_size.createCell(2).setCellValue(o.failed_small);

				counter++;
			}



			//totalloss
			int counter1=0;
			for(Double loss:fops.get(0).TotalLossesToBank) {
				Row row = sheet1.createRow((short) counter1);
				row.createCell(0).setCellValue(loss);
				counter1++;
			}

			//規模ごとdebtrank
			int c_mega= 0, c_medium = 0, c_small = 0;
			for(OutPutShort o:ops) {
				for(Institution fb: o.FailedBank) {
					if(fb.rank == "mega") {
						Row row = sheet3.createRow((short) c_mega);
						row.createCell(0).setCellValue(fb.dr);
						c_mega++;
					}
					if(fb.rank == "medium") {
						Row row = sheet4.createRow((short) c_medium);
						row.createCell(0).setCellValue(fb.dr);
						c_medium++;
					}
					if(fb.rank == "small") {
						Row row = sheet5.createRow((short) c_small);
						row.createCell(0).setCellValue(fb.dr);
						c_small++;
					}
				}
			}

			book.write(fileOut);
			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}



	private static void OutputNW(ArrayList<Institution> bank, String nw) {

		////NW analysis
		//calc comparator


		//pagerank
		Graph<Integer, Integer> g = new DirectedOrderedSparseMultigraph<Integer, Integer>();
		int edgenum=0;
		for (Institution b:bank) {
			g.addVertex(b.id);
			for(IBShort lb : b.ibsloan) {
				g.addEdge(edgenum ,b.id, lb.id, EdgeType.DIRECTED);
				edgenum++;
			}
		}
		PageRank<Integer,Integer> pr = new PageRank<Integer,Integer>(g,0.15);
		pr.evaluate();
		for (Integer i : g.getVertices()) {
			// i <- b.id
			for(Institution b:bank) {
				if(b.id == i) b.pr = pr.getVertexScore(i);
			}
		}

		//debtrank
		CalcDebtRank calcdr = new CalcDebtRank(bank);
		calcdr.isStart1();
		bank.stream().forEach(s -> s.dr = calcdr.getDebtRank(s.id));
		calcdr.clealList();

		//set relative assetsize
		double ta = 0;
		for(Institution b:bank) {
			ta += b.totalassets;
		}
		for(Institution b:bank) {
			b.rasize = b.totalassets/ta;
		}

		////output
		Workbook book =null;
		try {
		book = new HSSFWorkbook();
		String fileName = "workbookNWAnalysis"+nw+".xls";
		FileOutputStream fileOut = new FileOutputStream(fileName);
		String safename = WorkbookUtil.createSafeSheetName("[Result]");
		Sheet sheet = book.createSheet(safename);
		CreationHelper createHelper = book.getCreationHelper();

		// all bank with pagerank,debtrank,and relative assetsize in beginning
			Row row = sheet.createRow((short)0);
			Row row1 = sheet.createRow((short)1);
			Row row2 = sheet.createRow((short)2);
			Row row3 = sheet.createRow((short)3);
			Row row4 = sheet.createRow((short)4);
			row.createCell(0).setCellValue("id");
			row1.createCell(0).setCellValue("pr");
			row2.createCell(0).setCellValue("dr");
			row3.createCell(0).setCellValue("ra");
			row4.createCell(0).setCellValue("Assetsize");
		for(int i=0;i<bank.size();i++) {
			row.createCell(i+1).setCellValue(bank.get(i).id);
			row1.createCell(i+1).setCellValue(bank.get(i).pr);
			row2.createCell(i+1).setCellValue(bank.get(i).dr);
			row3.createCell(i+1).setCellValue(bank.get(i).rasize);
			row4.createCell(i+1).setCellValue(bank.get(i).totalassets);
		}

		book.write(fileOut);
		fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}

	private static void OutPutMiddleResult(ArrayList<MiddleOutPut> mop,String nw) {
		// TODO 自動生成されたメソッド・スタブ
		Workbook book = null;
		try {
			book = new HSSFWorkbook();
			String fileName = "workbookmiddle"+nw+".xls";
			FileOutputStream fileOut = new FileOutputStream(fileName);
			String safename = WorkbookUtil.createSafeSheetName("[ResultBank]");
			String safename1 = WorkbookUtil.createSafeSheetName("[NW]");
			Sheet sheet1 = book.createSheet(safename);
			Sheet sheet2 = book.createSheet(safename1);
			CreationHelper createHelper = book.getCreationHelper();

			//bank
			for (int x = 0; x < 1; x++) {
				Row row0 = sheet1.createRow((short) x);
				Row row1 = sheet1.createRow((short) x + 2);
				row1.createCell(0).setCellValue("CAR");
				Row row2 = sheet1.createRow((short) x + 3);
				Row row3 = sheet1.createRow((short) x + 4);
				row3.createCell(0).setCellValue("totalassets");
				Row row4 = sheet1.createRow((short) x + 5);
				Row row5 = sheet1.createRow((short) x + 6);
				row5.createCell(0).setCellValue("indegIB");
				Row row6 = sheet1.createRow((short) x + 7);
				Row row7 = sheet1.createRow((short) x + 8);
				row7.createCell(0).setCellValue("outdegIB");
				Row row8 = sheet1.createRow((short) x + 9);
				Row row9 = sheet1.createRow((short) x + 10);
				row9.createCell(0).setCellValue("degsumIB");
				Row row10 = sheet1.createRow((short) x + 11);
				Row row11 = sheet1.createRow((short) x + 12);
				row11.createCell(0).setCellValue("lf");
				Row row12 = sheet1.createRow((short) x + 13);
				Row row13 = sheet1.createRow((short) x + 14);
				row13.createCell(0).setCellValue("lb");
				Row row14 = sheet1.createRow((short) x + 15);
				Row row15 = sheet1.createRow((short) x + 16);
				row15.createCell(0).setCellValue("liqA");
				Row row16 = sheet1.createRow((short) x + 17);
				Row row17 = sheet1.createRow((short) x + 18);
				row17.createCell(0).setCellValue("buffer");
				Row row18 = sheet1.createRow((short) x + 19);
				Row row19 = sheet1.createRow((short) x + 20);
				row19.createCell(0).setCellValue("bb");
				Row row20 = sheet1.createRow((short) x + 21);
				Row row21 = sheet1.createRow((short) x + 22);
				row21.createCell(0).setCellValue("d");
				Row row22 = sheet1.createRow((short) x + 23);
				Row row23 = sheet1.createRow((short) x + 24);
				row23.createCell(0).setCellValue("n");
				Row row24 = sheet1.createRow((short) x + 25);
				Row row25 = sheet1.createRow((short) x + 26);
				row25.createCell(0).setCellValue("clientnum(toF)");
				Row row26 = sheet1.createRow((short) x + 27);
				Row row27 = sheet1.createRow((short) x + 28);
				row27.createCell(0).setCellValue("DebtRank");
				Row row28 = sheet1.createRow((short) x + 29);
				Row row29 = sheet1.createRow((short) x + 30);
				row29.createCell(0).setCellValue("PageRank");
				Row row30 = sheet1.createRow((short) x + 31);

				for (int i = 0; i < mop.size(); i++) {
					row0.createCell(i).setCellValue(i + 1);
					row2.createCell(i).setCellValue(mop.get(i).car);
					row4.createCell(i).setCellValue(mop.get(i).avgtotalassets);
					row6.createCell(i).setCellValue(mop.get(i).avgindegnum);
					row8.createCell(i).setCellValue(mop.get(i).avgoutdegnum);
					row10.createCell(i).setCellValue(mop.get(i).avgdegsum);
					row12.createCell(i).setCellValue(mop.get(i).avglf);
					row14.createCell(i).setCellValue(mop.get(i).avglb);
					row16.createCell(i).setCellValue(mop.get(i).avgliqA);
					row18.createCell(i).setCellValue(mop.get(i).avgbuffer);
					row20.createCell(i).setCellValue(mop.get(i).avgbb);
					row26.createCell(i).setCellValue(mop.get(i).avgclientnum);
					row26.createCell(i).setCellValue(mop.get(i).avgclientnum);
					row28.createCell(i).setCellValue(mop.get(i).dr);
					row30.createCell(i).setCellValue(mop.get(i).pr);
				}
			}

			//bank nw
			int counter=1;
			int countermega=1,countermedium=1,countersmall=1;
			Row rowa = sheet2.createRow((short)0);
			rowa.createCell(0).setCellValue("ra");
			rowa.createCell(1).setCellValue("dr");
			rowa.createCell(2).setCellValue("pr");
			rowa.createCell(3).setCellValue("deg");
			rowa.createCell(5).setCellValue("MegaDB");
			rowa.createCell(6).setCellValue("Assetsize");
			rowa.createCell(7).setCellValue("PageRank");
			rowa.createCell(9).setCellValue("MediumDB");
			rowa.createCell(10).setCellValue("Assetsize");
			rowa.createCell(11).setCellValue("PageRank");
			rowa.createCell(13).setCellValue("SmallDB");
			rowa.createCell(14).setCellValue("Assetsize");
			rowa.createCell(15).setCellValue("PageRank");
			for(int i=0;i<mop.size();i++) {
				for(int j=0;j<mop.get(i).DR.size();j++) {
					Row rowb = sheet2.createRow((short)counter);
					rowb.createCell(0).setCellValue(mop.get(i).RelAsset.get(j));
					rowb.createCell(1).setCellValue(mop.get(i).DR.get(j));
					rowb.createCell(2).setCellValue(mop.get(i).PR.get(j));
					rowb.createCell(3).setCellValue(mop.get(i).Deg.get(j));
					counter++;

					//by size
					if(mop.get(i).Rank.get(j) == "mega") {
						rowb.createCell(5).setCellValue(mop.get(i).Assetsize.get(j));
						rowb.createCell(6).setCellValue(mop.get(i).DR.get(j));
						rowb.createCell(7).setCellValue(mop.get(i).PR.get(j));
						countermega++;
					}
					if(mop.get(i).Rank.get(j) == "medium") {
						rowb.createCell(9).setCellValue(mop.get(i).Assetsize.get(j));
						rowb.createCell(10).setCellValue(mop.get(i).DR.get(j));
						rowb.createCell(11).setCellValue(mop.get(i).PR.get(j));
						countermedium++;
					}
					if(mop.get(i).Rank.get(j) == "small") {
						rowb.createCell(13).setCellValue(mop.get(i).Assetsize.get(j));
						rowb.createCell(14).setCellValue(mop.get(i).DR.get(j));
						rowb.createCell(15).setCellValue(mop.get(i).PR.get(j));
						countersmall++;
					}
				}
			}




			book.write(fileOut);
			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}

	private static void OutputMiddle(ArrayList<Institution> bank, ArrayList<MiddleOutPut> mop) {
		// TODO 自動生成されたメソッド・スタブ

		//NW analysis
		//pagerank
		Graph<Integer, Integer> g = new DirectedOrderedSparseMultigraph<Integer, Integer>();
		int edgenum=0;
		for (Institution b:bank) {
			g.addVertex(b.id);
			for(IBShort lb : b.ibsloan) {
				g.addEdge(edgenum ,b.id, lb.id, EdgeType.DIRECTED);
				edgenum++;
			}
		}
		PageRank<Integer,Integer> pr = new PageRank<Integer,Integer>(g,0.15);
		pr.evaluate();
		for (Integer i : g.getVertices()) {
			// i <- b.id
			for(Institution b:bank) {
				if(b.id == i) b.pr = pr.getVertexScore(i);
			}
		}

		//debtrank
		CalcDebtRank calcdr = new CalcDebtRank(bank);
		calcdr.isStart1();
		bank.stream().forEach(s -> s.dr = calcdr.getDebtRank(s.id));
		calcdr.clealList();

		//set relative assetsize
		double ta = 0;
		for(Institution b:bank) {
			ta += b.totalassets;
		}
		for(Institution b:bank) {
			b.rasize = b.totalassets/ta;
		}



		for(int i=0;i<mop.size();i++) {
			mop.get(i).setResultAndCalc(bank.get(i));
		}
	}

	private static void OutPutShock(ArrayList<ShockToBank> stb, int grossterm, int atp) {
		// TODO 自動生成されたメソッド・スタブ
		Workbook book = null;
		try {
			book = new HSSFWorkbook();
			String fileName = "workbookShock.xls";
			FileOutputStream fileOut = new FileOutputStream(fileName);
			String safename = WorkbookUtil.createSafeSheetName("[OutPutShock]");
			String safename1 = WorkbookUtil.createSafeSheetName("[OutPutShock1]");
			Sheet sheet = book.createSheet(safename);
			Sheet sheet1= book.createSheet(safename1);

			///初期ショックの伝搬
			//企業の破綻
			int cellcounter=0;
			for (int i=0;i<stb.size();i++) {
				Row row = sheet.createRow((short)cellcounter);
				row.createCell(0).setCellValue(i+"試行目");
				Row row1 = sheet.createRow((short)cellcounter+1);
				for(int j=0;j<stb.get(i).ffbb.size();j++) {
					row.createCell(j+1).setCellValue(stb.get(i).ffbb.get(j).rank);
					row1.createCell(j+1).setCellValue(stb.get(i).ffbb.get(j).grossl);
				}
				cellcounter+=2;
			}

			cellcounter=0;
			for (int i=0;i<stb.size();i++) {
				Row row = sheet1.createRow((short)cellcounter);
				row.createCell(0).setCellValue(i+"試行目");
				row.createCell(1).setCellValue("mega");
				Row row1 = sheet1.createRow((short)cellcounter+1);
				row1.createCell(1).setCellValue("medium");
				Row row2 = sheet1.createRow((short)cellcounter+2);
				row2.createCell(1).setCellValue("small");
				for(int j=0;j<stb.get(i).ffbb.size();j++) {
					if(stb.get(i).ffbb.get(j).rank=="mega")row.createCell(j+2).setCellValue(stb.get(i).ffbb.get(j).grossl);
					if(stb.get(i).ffbb.get(j).rank=="medium")row1.createCell(j+2).setCellValue(stb.get(i).ffbb.get(j).grossl);
					if(stb.get(i).ffbb.get(j).rank=="small")row2.createCell(j+2).setCellValue(stb.get(i).ffbb.get(j).grossl);
				}
				cellcounter+=3;
			}

			book.write(fileOut);
			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

	public static void OutPutShortResult(ArrayList<FinalOutPutShort> fops, int grossterm, int expterm, int atp,
			ArrayList<OutPutShort> ops, String a) {
		Workbook book = null;
		try {
			book = new HSSFWorkbook();
			String outputPath = "C:\\\\User\\\\tkfcb\\\\OneDrive\\\\OutPut\\\\";
			String fileName = "workbookExpShort" + a + ".xls";
			FileOutputStream fileOut = new FileOutputStream(fileName);
			String safename = WorkbookUtil.createSafeSheetName("[OutPutShort]");
			String safename1 = WorkbookUtil.createSafeSheetName("[OutPutShortLiveBank]");
			String safename2 = WorkbookUtil.createSafeSheetName("[LiveBank箱ひげ図用]");
			String safename3 = WorkbookUtil.createSafeSheetName("[PageRankwithAlive]");
			String safename4 = WorkbookUtil.createSafeSheetName("[DebtRank]");
			String safename5 = WorkbookUtil.createSafeSheetName("nwAggregate");
			String safename6 = WorkbookUtil.createSafeSheetName("Avg_DR&Alive_Bank");
			Sheet sheet = book.createSheet(safename);
			Sheet sheet1 = book.createSheet(safename1);
			Sheet sheet2 = book.createSheet(safename2);
			Sheet sheet3 = book.createSheet(safename3);
			Sheet sheet4 = book.createSheet(safename4);
			Sheet sheet5 = book.createSheet(safename5);
			Sheet sheet6 = book.createSheet(safename6);


			//破綻数　破綻要因 ...
			Row row = sheet.createRow((short) 0);
			row.createCell(0).setCellValue("総破綻数");
			Row row1 = sheet.createRow((short) 1);
			Row row3 = sheet.createRow((short) 2);
			row3.createCell(0).setCellValue("Both");
			Row row4 = sheet.createRow((short) 3);
			Row row5 = sheet.createRow((short) 4);
			row5.createCell(0).setCellValue("CAR");
			Row row6 = sheet.createRow((short) 5);
			Row row7 = sheet.createRow((short) 6);
			row7.createCell(0).setCellValue("Gap");
			Row row8 = sheet.createRow((short) 7);
			Row row9 = sheet.createRow((short) 8);
			row9.createCell(0).setCellValue("afterCM");
			Row row10 = sheet.createRow((short) 9);
			Row row11 = sheet.createRow((short) 10);
			row11.createCell(0).setCellValue("notrollover");
			Row row12 = sheet.createRow((short) 11);
			Row row13 = sheet.createRow((short) 12);
			row13.createCell(0).setCellValue("ibsupply");
			Row row14 = sheet.createRow((short) 13);
			Row row15 = sheet.createRow((short) 14);
			row15.createCell(0).setCellValue("alivebanknum");
			Row row16 = sheet.createRow((short) 15);
			Row row17 = sheet.createRow((short) 16);
			row17.createCell(0).setCellValue("FailedMega");
			Row row18 = sheet.createRow((short) 17);
			Row row19 = sheet.createRow((short) 18);
			row19.createCell(0).setCellValue("FailedMedium");
			Row row20 = sheet.createRow((short) 19);
			Row row21 = sheet.createRow((short) 20);
			row21.createCell(0).setCellValue("FailedSmall");
			Row row22 = sheet.createRow((short) 21);
			Row row23 = sheet.createRow((short) 22);
			row23.createCell(0).setCellValue("DR");
			Row row24 = sheet.createRow((short) 23);

			for (int i = 0; i < grossterm; i++) {
				row1.createCell(i).setCellValue(fops.get(0).AvgFailedBankNum.get(i));
				row4.createCell(i).setCellValue(fops.get(0).AvgFailedBankBoth.get(i));
				row6.createCell(i).setCellValue(fops.get(0).AvgFailedBankCAR.get(i));
				row8.createCell(i).setCellValue(fops.get(0).AvgFailedBankGap.get(i));
				row12.createCell(i).setCellValue(fops.get(0).AvgNotRollOver.get(i));
				row14.createCell(i).setCellValue(fops.get(0).AvgIBSupply.get(i));
				row16.createCell(i).setCellValue(fops.get(0).AvgAliveBankNum.get(i));
				row18.createCell(i).setCellValue(fops.get(0).AvgFailedBankMega.get(i));
				row20.createCell(i).setCellValue(fops.get(0).AvgFailedBankMedium.get(i));
				row22.createCell(i).setCellValue(fops.get(0).AvgFailedBankSmall.get(i));
				row24.createCell(i).setCellValue(fops.get(0).AvgDebtRank.get(i));
			}
			for (int i = 0; i < expterm; i++) {
				row10.createCell(i).setCellValue(fops.get(0).AvgFailedBankCM.get(i));
			}

			//タイムステップ　output
			//破綻数max min，規模ごとの破綻数
			ArrayList<Integer> brnumlist=new ArrayList<Integer>();
			for(int i=0;i<atp;i++) {
				brnumlist.add(ops.get(i).AliveBankNum.get(grossterm-1));
			}
			Integer max_alive=Collections.max(brnumlist);
			Integer min_alive=Collections.min(brnumlist);
			Row rowa=sheet1.createRow((short)0);
			rowa.createCell(0).setCellValue(max_alive);
			Row rowa1=sheet1.createRow((short)1);
			rowa1.createCell(0).setCellValue(min_alive);


			//試行ごと　timestep 箱ひげ図用
			int countcell = 0;
			for (int i = 0; i < grossterm; i++) {
				for (OutPutShort o : ops) {
					Row rowb = sheet2.createRow((short) countcell);
					rowb.createCell(0).setCellValue(i + 1);
					rowb.createCell(1).setCellValue(o.AliveBankNum.get(i));
					countcell++;
				}
			}

			//試行ごと pagerank output
			int countcell1 = 0;
			int counter = 1;
			for(OutPutShort o : ops) {
				Row rowc = sheet3.createRow((short)countcell1);
				Row rowc1 = sheet3.createRow((short)countcell1+1);
				Row rowc2 = sheet3.createRow((short)countcell1+2);
				Row rowc3 = sheet3.createRow((short)countcell1+3);
				rowc.createCell(0).setCellValue((counter+1)+"atp");
				counter++;
				for (int i=0;i<grossterm ; i++) {
					rowc.createCell(i+1).setCellValue(i+"step");
					rowc1.createCell(i+1).setCellValue(o.DifPageRank.get(i));
					rowc2.createCell(i+1).setCellValue(o.MaxDifPageRank.get(i));
					rowc3.createCell(i+1).setCellValue(o.MinDifPageRank.get(i));
				}
				countcell1+=4;
			}

			//各試行ごと　時系列
			int countcell2 = 0;
			int counter2 = 1;
			for(OutPutShort o : ops) {
				Row rowc = sheet4.createRow((short)countcell2);
				Row rowc1 = sheet4.createRow((short)countcell2+1);
				Row rowc2 = sheet4.createRow((short)countcell2+2);
				Row rowc3 = sheet4.createRow((short)countcell2+3);
				rowc.createCell(0).setCellValue((counter2)+"atp");
				rowc1.createCell(1).setCellValue("DebtRank");
				rowc2.createCell(1).setCellValue("AssetSize");
				rowc3.createCell(1).setCellValue("PageRank");
				counter2++;
				for(int i=0;i< o.FailedBank.size();i++) {
					rowc.createCell(i+2).setCellValue(o.FailedBank.get(i).id+"銀行");
					rowc1.createCell(i+2).setCellValue(o.FailedBank.get(i).dr);
					rowc2.createCell(i+2).setCellValue(o.FailedBank.get(i).totalassets);
					rowc3.createCell(i+2).setCellValue(o.FailedBank.get(i).pr);
				}
				countcell2+=4;
			}



			//全試行　all default bank with debtrank,assetsize and pagerank, by size
			int countcell3 = 1;
			int countcellmega = 1;
			int countcellmedium = 1;
			int countcellsmall = 1;

			Row rowd = sheet5.createRow((short)0);
			rowd.createCell(0).setCellValue("DebtRank");
			rowd.createCell(1).setCellValue("AssetSize");
			rowd.createCell(2).setCellValue("PageRank");
			rowd.createCell(5).setCellValue("MegaDB");
			rowd.createCell(6).setCellValue("Assetsize");
			rowd.createCell(7).setCellValue("PageRank");
			rowd.createCell(9).setCellValue("MediumDB");
			rowd.createCell(10).setCellValue("Assetsize");
			rowd.createCell(11).setCellValue("PageRank");
			rowd.createCell(13).setCellValue("SmallDB");
			rowd.createCell(14).setCellValue("Assetsize");
			rowd.createCell(15).setCellValue("PageRank");
			for(OutPutShort o:ops) {
				System.out.println("FailedBankSize "+o.FailedBank.size());

				//all default bank
				for(Institution fb:o.FailedBank) {
					Row rowd1 = sheet5.createRow((short) countcell3);
					rowd1.createCell(0).setCellValue(fb.dr);
					rowd1.createCell(1).setCellValue(fb.totalassets);
					rowd1.createCell(2).setCellValue(fb.pr);
					countcell3++;

					//by size
					if(fb.rank == "mega") {
						rowd1.createCell(5).setCellValue(fb.dr);
						rowd1.createCell(6).setCellValue(fb.totalassets);
						rowd1.createCell(7).setCellValue(fb.pr);
						countcellmega++;
					}
					if(fb.rank == "medium") {
						rowd1.createCell(9).setCellValue(fb.dr);
						rowd1.createCell(10).setCellValue(fb.totalassets);
						rowd1.createCell(11).setCellValue(fb.pr);
						countcellmedium++;
					}
					if(fb.rank == "small") {
						rowd1.createCell(13).setCellValue(fb.dr);
						rowd1.createCell(14).setCellValue(fb.totalassets);
						rowd1.createCell(15).setCellValue(fb.pr);
						countcellsmall++;
					}
				}
			}

			//time series Average DebtRank and num of alive bank
			int countcelldr = 0;
			int counterdr = 1;
			for(OutPutShort o : ops) {
				Row rowdr = sheet6.createRow((short)countcelldr);
				Row rowdr1 = sheet6.createRow((short)countcelldr+1);
				Row rowdr2 = sheet6.createRow((short)countcelldr+2);
				rowdr.createCell(0).setCellValue((counterdr)+"atp");
				counterdr++;
				for (int i=0;i<grossterm ; i++) {
					rowdr.createCell(i+1).setCellValue(i+"step");
					rowdr1.createCell(i+1).setCellValue(o.AvgDebtRank.get(i));
					rowdr2.createCell(i+1).setCellValue(o.AliveBankNum.get(i));
				}
				countcelldr+=3;
			}


			book.write(fileOut);
			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}

	private static void OutPutConditions(int banknode, int firmnode, double r, int tau, int term, double z, double xx,
			double psy, double alpha, double myu, double sita, double fi, double g, double c, double lamda,
			int initialbanknumber, int d, double e, double k0, double dr, int startterm, double div, double liqAr,
			int creditterm, double bscallrate, double rz, double beta, double h, double sm, double lfmin, double lfmax,
			double val, double pi, double vi, int candmax, double rr, double E0, double f, double a1, double a2, int la,
			int megala, int mediumla, int smallla, double theta, double randIn, double randOut) {
		// TODO 自動生成されたメソッド・スタブ

		Workbook book = null;

		try {

			book = new HSSFWorkbook();
			FileOutputStream fileOut = new FileOutputStream("workbookConditions.xls");
			String safename = WorkbookUtil.createSafeSheetName("[Conditions]");
			Sheet sheet1 = book.createSheet(safename);
			CreationHelper createHelper = book.getCreationHelper();

			int x = 0;
			Row row0 = sheet1.createRow((short) x);
			row0.createCell(0).setCellValue("実験条件");
			Row row1 = sheet1.createRow((short) x + 1);
			row1.createCell(0).setCellValue("Bank");
			Row row2 = sheet1.createRow((short) x + 2);
			Row row3 = sheet1.createRow((short) x + 3);
			row2.createCell(0).setCellValue("node");
			row3.createCell(0).setCellValue(banknode);
			row2.createCell(1).setCellValue("初期合計Asset");
			row3.createCell(1).setCellValue(E0);
			row2.createCell(2).setCellValue("初期CAR");
			row3.createCell(2).setCellValue("0.15,0.11,0.09");
			row2.createCell(3).setCellValue("LiqA保持率");
			row3.createCell(3).setCellValue("50-70%");
			row2.createCell(4).setCellValue("μ");
			row3.createCell(4).setCellValue(myu);
			row2.createCell(5).setCellValue("θ");
			row3.createCell(5).setCellValue(sita);
			row2.createCell(6).setCellValue("ρ");
			row3.createCell(6).setCellValue(xx);
			row2.createCell(7).setCellValue("о");
			row3.createCell(7).setCellValue(psy);
			row2.createCell(8).setCellValue("配当率");
			row3.createCell(8).setCellValue(div);

			Row row4 = sheet1.createRow((short) x + 5);
			row4.createCell(0).setCellValue("Firm");
			Row row5 = sheet1.createRow((short) x + 6);
			Row row6 = sheet1.createRow((short) x + 7);
			row5.createCell(0).setCellValue("node");
			row6.createCell(0).setCellValue(firmnode);
			row5.createCell(1).setCellValue("初期Asset");
			row6.createCell(1).setCellValue(k0);
			row5.createCell(2).setCellValue("LevSet");
			row6.createCell(2).setCellValue(lfmin + " " + lfmax);
			row5.createCell(3).setCellValue("生産関数係数");
			row6.createCell(3).setCellValue(fi);
			row5.createCell(4).setCellValue("生産関数乗数");
			row6.createCell(4).setCellValue(beta);
			row5.createCell(5).setCellValue("m");
			row6.createCell(5).setCellValue(0.21);
			row5.createCell(6).setCellValue("ε");
			row6.createCell(6).setCellValue(0.04);
			row5.createCell(7).setCellValue("生産コスト係数(自己資本比例型)");
			row6.createCell(7).setCellValue(f);
			row5.createCell(8).setCellValue("σ");
			row6.createCell(8).setCellValue(1);
			row5.createCell(9).setCellValue("金利係数(lev比例型)");
			row6.createCell(9).setCellValue(rr);

			Row row7 = sheet1.createRow((short) x + 9);
			row7.createCell(0).setCellValue("Learning");
			Row row8 = sheet1.createRow((short) x + 10);
			Row row9 = sheet1.createRow((short) x + 11);
			row8.createCell(0).setCellValue("割引率");
			row9.createCell(0).setCellValue(sm);
			row8.createCell(1).setCellValue("forgatting");
			row9.createCell(1).setCellValue(h);
			row8.createCell(2).setCellValue("g");
			row9.createCell(2).setCellValue(0.1);
			row8.createCell(3).setCellValue("Strength");
			row9.createCell(3).setCellValue(vi);

			Row row10 = sheet1.createRow((short) x + 12);
			row10.createCell(0).setCellValue("市場環境");
			Row row11 = sheet1.createRow((short) x + 13);
			Row row12 = sheet1.createRow((short) x + 14);
			row11.createCell(0).setCellValue("市場金利");
			row12.createCell(0).setCellValue(sm);
			row11.createCell(1).setCellValue("返済期間");
			row12.createCell(1).setCellValue(tau);
			row11.createCell(2).setCellValue("ステップ");
			row12.createCell(2).setCellValue(term);
			row11.createCell(3).setCellValue("市場金利");
			row12.createCell(3).setCellValue(r);
			row11.createCell(4).setCellValue("預金金利");
			row12.createCell(4).setCellValue(dr);
			row11.createCell(5).setCellValue("市場性資産収益");
			row12.createCell(5).setCellValue(liqAr);

			Row row13 = sheet1.createRow((short) x + 12);
			row13.createCell(0).setCellValue("network・asset");
			Row row14 = sheet1.createRow((short) x + 13);
			Row row15 = sheet1.createRow((short) x + 14);
			row14.createCell(0).setCellValue("..megabank");
			row15.createCell(0).setCellValue(a1);
			row14.createCell(1).setCellValue("..mediumbank");
			row15.createCell(1).setCellValue(a2);
			row14.createCell(2).setCellValue("ExassetNum");
			row15.createCell(2).setCellValue(la);
			row14.createCell(3).setCellValue("megala");
			row15.createCell(3).setCellValue(megala);
			row14.createCell(4).setCellValue("mediumla");
			row15.createCell(4).setCellValue(mediumla);
			row14.createCell(5).setCellValue("smallla");
			row15.createCell(5).setCellValue(smallla);
			row14.createCell(6).setCellValue("BA・連結度");
			row15.createCell(6).setCellValue(theta);
			row14.createCell(7).setCellValue("CP・randIn");
			row15.createCell(7).setCellValue(randIn);
			row14.createCell(8).setCellValue("CP・randOut");
			row15.createCell(8).setCellValue(randOut);

			book.write(fileOut);
			fileOut.close();

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {

		}

	}

	private static void InitBankOutput(ArrayList<OutPut> output, ArrayList<Institution> initBank, int y) {
		// TODO 自動生成されたメソッド・スタブ

		//総次数順に並び替え
		//Collections.sort(initBank,(p1,p2)->p2.deg-p1.deg);

		output.get(y).Initbank.addAll(initBank);

	}

	private static void ResultAdjustRes(ArrayList<FinalOutPut> finaloutput, ArrayList<OutPut> output, int term, int atp,
			int firmnode) {
		// TODO 自動生成されたメソッド・スタブ
		finaloutput.get(0).Calc(output, firmnode);
	}

	private static void ResultAdjustExp(ArrayList<FinalOutPutShort> finaloutput, ArrayList<OutPutShort> outputShort) {
		// TODO 自動生成されたメソッド・スタブ
		finaloutput.get(0).Calc(outputShort);
	}

	private static void ResultAdjustExp1(ArrayList<FinalOutPutShort> finaloutput, ArrayList<OutPutShort> outputShort) {
		// TODO 自動生成されたメソッド・スタブ
		finaloutput.get(0).Calc(outputShort);
	}

	private static void AgentClear(ArrayList<Institution> bank, ArrayList<Enterprise> firm, ArrayList<BrProcess> vand,
			ArrayList<Institution> initBank, ArrayList<Institution> xBank, ArrayList<Enterprise> xFirm,
			ArrayList<BrProcess> xVand, ArrayList<Asset> asset, ArrayList<Asset> xAsset, ArrayList<Institution> yBank,
			ArrayList<Enterprise> yFirm, ArrayList<Asset> yAsset, ArrayList<BrProcess> yVand, ArrayList<Institution> zBank, ArrayList<Enterprise> zFirm, ArrayList<Asset> zAsset, ArrayList<BrProcess> zVand) {
		// TODO 自動生成されたメソッド・スタブ
		bank.clear();
		firm.clear();
		vand.clear();
		initBank.clear();
		xBank.clear();
		xFirm.clear();
		xVand.clear();
		asset.clear();
		xAsset.clear();
		yBank.clear();
		yFirm.clear();
		yAsset.clear();
		yVand.clear();
		zBank.clear();
		zFirm.clear();
		zAsset.clear();
		zVand.clear();
	}

	private static void showAgent(Experiment e1, ArrayList<Enterprise> firm) {

		// TODO 自動生成されたメソッド・スタブ
		Workbook book = null;

		//ある企業エージェントの時系列推移
		int ag = 0;//企業エージェントリストindex
		//資産規模順にソート
		e1.setSortFirmK();

		try {
			book = new HSSFWorkbook();
			FileOutputStream fileOut = new FileOutputStream("workbookAgent.xls");
			String safename = WorkbookUtil.createSafeSheetName("[One Firm]");
			Sheet sheet1 = book.createSheet(safename);
			CreationHelper createHelper = book.getCreationHelper();
			int x = 0;
			Row row0 = sheet1.createRow((short) x);
			row0.createCell(0).setCellValue("企業" + e1.getOneFirmId(ag));
			Row row1 = sheet1.createRow((short) x + 2);
			row1.createCell(0).setCellValue("Leverage");
			Row row3 = sheet1.createRow((short) x + 8);
			row3.createCell(0).setCellValue("総資産");

			Row row5 = sheet1.createRow((short) x + 14);
			row5.createCell(0).setCellValue("選択レバレッジ");

			Row row7 = sheet1.createRow((short) x + 20);
			row7.createCell(0).setCellValue("取引先数");

			Row row9 = sheet1.createRow((short) x + 26);
			row9.createCell(0).setCellValue("収益");
			Row row11 = sheet1.createRow((short) x + 32);
			row11.createCell(0).setCellValue("選択レバレッジ値");

			Row row13 = sheet1.createRow((short) x + 38);
			row13.createCell(0).setCellValue("Reallev(afterCM)");

			int endterm = e1.getOneFirmLived(ag) - 1;

			int rowterm = endterm / 100 + 1;
			int term = endterm;
			int xterm = 100;
			int a = 0, b = xterm - 1;

			for (; x < rowterm; x++) {
				Row row2 = sheet1.createRow((short) (x + 3));
				Row row4 = sheet1.createRow((short) (x + 9));
				Row row6 = sheet1.createRow((short) (x + 15));
				Row row8 = sheet1.createRow((short) (x + 21));
				Row row10 = sheet1.createRow((short) (x + 27));
				Row row12 = sheet1.createRow((short) (x + 33));
				Row row14 = sheet1.createRow((short) (x + 39));

				int j = 0;
				for (j = a; j <= b; j++) {
					row2.createCell(j - (x * xterm)).setCellValue(e1.getOneFirmLev(ag, j));
					row4.createCell(j - (x * xterm)).setCellValue(e1.getOneFirmK(ag, j));
					row6.createCell(j - (x * xterm)).setCellValue(e1.getOneFirmGoalLev(ag, j));
					row8.createCell(j - (x * xterm)).setCellValue(e1.getOneFirmCandNum(ag, j));
					row10.createCell(j - (x * xterm)).setCellValue(e1.getOneFirmPai(ag, j));
					row12.createCell(j - (x * xterm)).setCellValue(e1.getOneFirmLevChoice(ag, j));
					row14.createCell(j - (x * xterm)).setCellValue(e1.getOneFirmRealLev(ag, j));

					//cell.setCellValue(createHelper.createRichTextString("sample String"));
				}

				if (term > 100) {
					if (term - 1 > xterm + j) {
						if (term - j - 1 > xterm) {
							a = j;
							b = j + xterm - 1;
						}
					} else {
						a = j;
						b = term - 1;
					}

				}
			}
			book.write(fileOut);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

	private static void OutPutResultInit(ArrayList<FinalOutPut> finalOutput, int term, int atp, int firmnode,
			int banknode) {
		// TODO 自動生成されたメソッド・スタブ
		Workbook book = null;

		try {
			book = new HSSFWorkbook();
			String fileName = "workbookInit.xls";
			FileOutputStream fileOut = new FileOutputStream(fileName);
			String safename = WorkbookUtil.createSafeSheetName("[Bank]");
			Sheet sheet1 = book.createSheet(safename);

			//初期銀行

			for (int x = 0; x < 1; x++) {
				Row row0 = sheet1.createRow((short) x);
				Row row1 = sheet1.createRow((short) x + 3);
				row1.createCell(0).setCellValue("銀行資産規模K");
				Row row2 = sheet1.createRow((short) x + 4);
				Row row3 = sheet1.createRow((short) x + 5);
				row3.createCell(0).setCellValue("Inid");
				Row row4 = sheet1.createRow((short) x + 6);
				Row row5 = sheet1.createRow((short) x + 7);
				row5.createCell(0).setCellValue("Outid");
				Row row6 = sheet1.createRow((short) x + 8);
				Row row7 = sheet1.createRow((short) x + 9);
				row7.createCell(0).setCellValue("Deg");
				Row row8 = sheet1.createRow((short) x + 10);
				Row row9 = sheet1.createRow((short) x + 11);
				row9.createCell(0).setCellValue("firmclient");
				Row row10 = sheet1.createRow((short) x + 12);

				for (int i = 0; i < banknode; i++) {
					row0.createCell(i).setCellValue(i + 1);
					row2.createCell(i).setCellValue(finalOutput.get(0).AvgbankinitK.get(i));
					row4.createCell(i).setCellValue(finalOutput.get(0).AvgbankinitInid.get(i));
					row6.createCell(i).setCellValue(finalOutput.get(0).AvgbankinitOutid.get(i));
					row8.createCell(i).setCellValue(finalOutput.get(0).Avgbankdeg.get(i));
				}

			}

			book.write(fileOut);
			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

	private static void OutPutMiddleResult(ArrayList<MiddleOutPut> mop, ArrayList<MiddleOutPutFirm> mopf,
			ArrayList<Correlation> cor) {

		Workbook book = null;

		try {
			//c:\\User\tkfcb\Onedrive\OutPut\workbook.xls

			book = new HSSFWorkbook();
			String outputPath = "C:\\\\User\\\\tkfcb\\\\OneDrive\\\\OutPut\\\\";
			String fileName = "workbookmiddle.xls";
			FileOutputStream fileOut = new FileOutputStream(fileName);
			String safename = WorkbookUtil.createSafeSheetName("[ResultBank]");
			String safename1 = WorkbookUtil.createSafeSheetName("[ResultFirm]");
			String safename2 = WorkbookUtil.createSafeSheetName("[ResultCorelation]");
			Sheet sheet1 = book.createSheet(safename);
			Sheet sheet2 = book.createSheet(safename1);
			Sheet sheet3 = book.createSheet(safename2);
			CreationHelper createHelper = book.getCreationHelper();

			//bank
			for (int x = 0; x < 1; x++) {
				Row row0 = sheet1.createRow((short) x);
				Row row1 = sheet1.createRow((short) x + 2);
				row1.createCell(0).setCellValue("CAR");
				Row row2 = sheet1.createRow((short) x + 3);
				Row row3 = sheet1.createRow((short) x + 4);
				row3.createCell(0).setCellValue("totalassets");
				Row row4 = sheet1.createRow((short) x + 5);
				Row row5 = sheet1.createRow((short) x + 6);
				row5.createCell(0).setCellValue("indegIB");
				Row row6 = sheet1.createRow((short) x + 7);
				Row row7 = sheet1.createRow((short) x + 8);
				row7.createCell(0).setCellValue("outdegIB");
				Row row8 = sheet1.createRow((short) x + 9);
				Row row9 = sheet1.createRow((short) x + 10);
				row9.createCell(0).setCellValue("degsumIB");
				Row row10 = sheet1.createRow((short) x + 11);
				Row row11 = sheet1.createRow((short) x + 12);
				row11.createCell(0).setCellValue("lf");
				Row row12 = sheet1.createRow((short) x + 13);
				Row row13 = sheet1.createRow((short) x + 14);
				row13.createCell(0).setCellValue("lb");
				Row row14 = sheet1.createRow((short) x + 15);
				Row row15 = sheet1.createRow((short) x + 16);
				row15.createCell(0).setCellValue("liqA");
				Row row16 = sheet1.createRow((short) x + 17);
				Row row17 = sheet1.createRow((short) x + 18);
				row17.createCell(0).setCellValue("buffer");
				Row row18 = sheet1.createRow((short) x + 19);
				Row row19 = sheet1.createRow((short) x + 20);
				row19.createCell(0).setCellValue("bb");
				Row row20 = sheet1.createRow((short) x + 21);
				Row row21 = sheet1.createRow((short) x + 22);
				row21.createCell(0).setCellValue("d");
				Row row22 = sheet1.createRow((short) x + 23);
				Row row23 = sheet1.createRow((short) x + 24);
				row23.createCell(0).setCellValue("n");
				Row row24 = sheet1.createRow((short) x + 25);
				Row row25 = sheet1.createRow((short) x + 26);
				row25.createCell(0).setCellValue("clientnum(toF)");
				Row row26 = sheet1.createRow((short) x + 27);
				Row row27 = sheet1.createRow((short) x + 28);

				for (int i = 0; i < mop.size(); i++) {
					row0.createCell(i).setCellValue(i + 1);
					row2.createCell(i).setCellValue(mop.get(i).car);
					row4.createCell(i).setCellValue(mop.get(i).avgtotalassets);
					row6.createCell(i).setCellValue(mop.get(i).avgindegnum);
					row8.createCell(i).setCellValue(mop.get(i).avgoutdegnum);
					row10.createCell(i).setCellValue(mop.get(i).avgdegsum);
					row12.createCell(i).setCellValue(mop.get(i).avglf);
					row14.createCell(i).setCellValue(mop.get(i).avglb);
					row16.createCell(i).setCellValue(mop.get(i).avgliqA);
					row18.createCell(i).setCellValue(mop.get(i).avgbuffer);
					row20.createCell(i).setCellValue(mop.get(i).avgbb);
					row26.createCell(i).setCellValue(mop.get(i).avgclientnum);

				}
			}
			//firm
			int rowfirm = mopf.size() / 100;

			int fn = 100;
			int c = 0, d = fn - 1;
			int u = 0;

			//calc 相関係数
			ArrayList<Double> K = new ArrayList<Double>();
			ArrayList<Double> D = new ArrayList<Double>();
			ArrayList<Double> Deg = new ArrayList<Double>();
			double coK_D, coD_Deg, coK_Deg;
			for (int i = 0; i < mopf.size(); i++) {
				K.add(mopf.get(i).avgk);
				D.add(mopf.get(i).avgl);
				Deg.add(mopf.get(i).avgdeg);
			}
			Correlatin_Calculater corcalc = new Correlatin_Calculater();
			coK_D = corcalc.correlationCoefficient(K, D);
			coD_Deg = corcalc.correlationCoefficient(D, Deg);
			coK_Deg = corcalc.correlationCoefficient(K, Deg);
			Row rowa1 = sheet3.createRow((short) 0);
			rowa1.createCell(0).setCellValue("K_D");
			rowa1.createCell(1).setCellValue(coK_D);
			Row rowa2 = sheet3.createRow((short) 1);
			rowa2.createCell(0).setCellValue("D_Deg");
			rowa2.createCell(1).setCellValue(coD_Deg);
			Row rowa3 = sheet3.createRow((short) 2);
			rowa3.createCell(0).setCellValue("K_Deg");
			rowa3.createCell(1).setCellValue(coK_Deg);

			//各試行で相関をとって平均を出す
			double avgcoK_D = 0, avgcoD_Deg = 0, avgcoK_Deg = 0;
			for (Correlation C : cor) {
				avgcoK_D += C.coK_D / C.atp;
				avgcoD_Deg += C.coD_Deg / C.atp;
				avgcoK_Deg += C.coK_Deg / C.atp;
			}

			Row rowa4 = sheet3.createRow((short) 4);
			rowa4.createCell(0).setCellValue("K_D");
			rowa4.createCell(1).setCellValue(avgcoK_D);
			Row rowa5 = sheet3.createRow((short) 5);
			rowa5.createCell(0).setCellValue("D_Deg");
			rowa5.createCell(1).setCellValue(avgcoD_Deg);
			Row rowa6 = sheet3.createRow((short) 6);
			rowa6.createCell(0).setCellValue("K_Deg");
			rowa6.createCell(1).setCellValue(avgcoK_Deg);

			Row row00 = sheet2.createRow((short) u);
			row00.createCell(0).setCellValue("総資産");
			Row row02 = sheet2.createRow((short) u + 15);
			row02.createCell(0).setCellValue("mainbank数");
			Row row04 = sheet2.createRow((short) u + 30);
			row04.createCell(0).setCellValue("Leverage");
			Row row08 = sheet2.createRow((short) u + 45);
			row08.createCell(0).setCellValue("reallev");
			Row rowx1 = sheet2.createRow((short) u + 60);
			rowx1.createCell(0).setCellValue("l");
			Row rowx3 = sheet2.createRow((short) u + 75);
			rowx3.createCell(0).setCellValue("A");
			Row rowx5 = sheet2.createRow((short) u + 90);
			rowx5.createCell(0).setCellValue("maxmainbank");
			Row rowx7 = sheet2.createRow((short) u + 105);
			rowx7.createCell(0).setCellValue("mainbanknum(降順)");
			for (int x = 0; x < rowfirm; x++) {
				Row row1f = sheet2.createRow((short) x + 1);//行
				Row row3f = sheet2.createRow((short) x + 16);
				Row row5f = sheet2.createRow((short) x + 31);
				Row row7f = sheet2.createRow((short) x + 46);
				Row row9f = sheet2.createRow((short) x + 61);
				Row row1g = sheet2.createRow((short) x + 76);
				Row row3g = sheet2.createRow((short) x + 91);
				Row row5g = sheet2.createRow((short) x + 106);

				int j = 0;
				for (j = c; j <= d; j++) {
					//System.out.print(d);
					row1f.createCell(j - (x * fn)).setCellValue(mopf.get(j).avgk);
					row3f.createCell(j - (x * fn)).setCellValue(mopf.get(j).avgdeg);
					row5f.createCell(j - (x * fn)).setCellValue(mopf.get(j).avglev);
					row7f.createCell(j - (x * fn)).setCellValue(mopf.get(j).avgreallev);
					row9f.createCell(j - (x * fn)).setCellValue(mopf.get(j).avgl);
					row1g.createCell(j - (x * fn)).setCellValue(mopf.get(j).avgn);
					row3g.createCell(j - (x * fn)).setCellValue(mopf.get(j).avgmaxmainbank);
					//row5g.createCell(j - (x * fn)).setCellValue(mopf.get(j).avgmainbank);
					//row7.createCell(j-(x*fn)).setCellValue(e1.getFirmnextLeverage(j));
				}

				if (mopf.size() - 1 > fn + j) {
					if (mopf.size() - j - 1 > fn) {
						c = j;
						d = j + fn - 1;
					}
				} else {
					c = j;
					d = mopf.size() - 1;
				}
			}

			book.write(fileOut);
			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

	private static void OutPutResResult(ArrayList<FinalOutPut> finaloutput, int term, int atp, int firmnode,
			int banknode) {
		// TODO 自動生成されたメソッド・スタブ

		Workbook book = null;

		try {
			//c:\\User\tkfcb\Onedrive\OutPut\workbook.xls

			book = new HSSFWorkbook();
			String outputPath = "C:\\\\User\\\\tkfcb\\\\OneDrive\\\\OutPut\\\\";
			String fileName = "workbook.xls";
			FileOutputStream fileOut = new FileOutputStream(fileName);
			String safename = WorkbookUtil.createSafeSheetName("[OutPutResult1]");
			String safename1 = WorkbookUtil.createSafeSheetName("[BankResult]");
			String safename2 = WorkbookUtil.createSafeSheetName("[FirmResult]");
			String safename3 = WorkbookUtil.createSafeSheetName("[InitBank]");
			String safename4 = WorkbookUtil.createSafeSheetName("[SortFirmDeg]");
			Sheet sheet1 = book.createSheet(safename);
			Sheet sheet2 = book.createSheet(safename1);
			Sheet sheet3 = book.createSheet(safename2);
			Sheet sheet4 = book.createSheet(safename3);
			Sheet sheet5 = book.createSheet(safename4);
			CreationHelper createHelper = book.getCreationHelper();

			//100タームごとに改行
			int rowterm = term / 100 + 1;
			int xterm = 100;
			int a = 0, b = xterm - 1;
			//if(e1.getTerm()>100) {xterm=100;a=0;b=xterm-1;}else {xterm=0;a=0;b=e1.getTerm()-1;}
			int t = 0;
			Row row = sheet1.createRow((short) t);
			row.createCell(0).setCellValue("総生産");
			Row row2 = sheet1.createRow((short) t + 8);
			row2.createCell(0).setCellValue("生産高成長率");
			Row row4 = sheet1.createRow((short) t + 17);
			row4.createCell(0).setCellValue("企業破産数");
			Row row6 = sheet1.createRow((short) t + 27);
			row6.createCell(0).setCellValue("平均約定金利");
			Row row8 = sheet1.createRow((short) t + 36);
			row8.createCell(0).setCellValue("貸出拒否数");
			Row row10 = sheet1.createRow((short) t + 45);
			row10.createCell(0).setCellValue("企業レバレッジ");
			Row row12 = sheet1.createRow((short) t + 54);
			row12.createCell(0).setCellValue("銀行クライアント数");
			Row row14 = sheet1.createRow((short) t + 63);
			row14.createCell(0).setCellValue("全銀行総利益");
			Row row16 = sheet1.createRow((short) t + 72);
			row16.createCell(0).setCellValue("全銀行buffer");

			for (int x = 0; x < rowterm; x++) {

				Row row1 = sheet1.createRow((short) x + 1);

				Row row3 = sheet1.createRow((short) x + 9);

				Row row5 = sheet1.createRow((short) x + 18);

				Row row7 = sheet1.createRow((short) x + 28);

				Row row9 = sheet1.createRow((short) x + 37);

				Row row11 = sheet1.createRow((short) x + 46);

				Row row13 = sheet1.createRow((short) x + 55);
				Row row15 = sheet1.createRow((short) x + 64);
				Row row17 = sheet1.createRow((short) x + 73);
				int j = 0;
				for (j = a; j <= b; j++) {
					row1.createCell(j - (x * xterm)).setCellValue(finaloutput.get(0).Grossproduction.get(j));
					row3.createCell(j - (x * xterm)).setCellValue(finaloutput.get(0).Productgrowthrate.get(j));
					row5.createCell(j - (x * xterm)).setCellValue(finaloutput.get(0).FFnum.get(j));
					row7.createCell(j - (x * xterm)).setCellValue(finaloutput.get(0).Avginterest.get(j));
					row9.createCell(j - (x * xterm)).setCellValue(finaloutput.get(0).Rejectfinancing.get(j));
					row11.createCell(j - (x * xterm)).setCellValue(finaloutput.get(0).Avgfirmlev.get(j));
					row13.createCell(j - (x * xterm)).setCellValue(finaloutput.get(0).Avgbankclientnum.get(j));
					row15.createCell(j - (x * xterm)).setCellValue(finaloutput.get(0).Grossbankprofit.get(j));
					row17.createCell(j - (x * xterm)).setCellValue(finaloutput.get(0).Grossbankbuffer.get(j));

				}
				if (term > 100) {
					if (term - 1 > xterm + j) {
						if (term - j - 1 > xterm) {
							a = j;
							b = j + xterm - 1;
						}
					} else {
						a = j;
						b = term - 1;
					}

				}
			}

			//企業
			int rowfirm = firmnode / 100;

			int fn = 100;
			int c = 0, d = fn - 1;
			int u = 0;

			//mainbank順に並べたときの標準偏差
			ArrayList<FinalOutputFirm> FOF = new ArrayList<FinalOutputFirm>();
			for (int i = 0; i < firmnode; i++) {
				FinalOutputFirm A = new FinalOutputFirm(i, finaloutput.get(0).Avgfirmmainbank.get(i),
						finaloutput.get(0).SDfirmmainbank.get(i));
				FOF.add(A);
			}
			//degree順にソート
			ArrayList<FinalOutputFirm> FOFdeg = new ArrayList<FinalOutputFirm>();
			FOF.stream().sorted((Comparator.comparingDouble(FinalOutputFirm::getAvgDegree)).reversed())
					.forEach(s -> FOFdeg.add(s));

			Row row00 = sheet3.createRow((short) u);
			row00.createCell(0).setCellValue("総資産");
			Row row02 = sheet3.createRow((short) u + 15);
			row02.createCell(0).setCellValue("mainbank数");
			Row row04 = sheet3.createRow((short) u + 30);
			row04.createCell(0).setCellValue("Leverage");
			Row row06 = sheet3.createRow((short) u + 45);
			row06.createCell(0).setCellValue("mainbank標準偏差");

			Row row08 = sheet3.createRow((short) u + 60);
			row08.createCell(0).setCellValue("成長率");
			Row rowx1 = sheet3.createRow((short) u + 75);
			rowx1.createCell(0).setCellValue("reallev(afterCM)");
			Row rowx3 = sheet3.createRow((short) u + 90);
			rowx3.createCell(0).setCellValue("inMarketLive");
			Row rowx5 = sheet3.createRow((short) u + 105);
			rowx5.createCell(0).setCellValue("sortDeg");
			for (int x = 0; x < rowfirm; x++) {
				/*
					Row row0= sheet3.createRow((short)x);
					row0.createCell(0).setCellValue("総資産");
					*/
				Row row1f = sheet3.createRow((short) x + 1);//行
				Row row3f = sheet3.createRow((short) x + 16);
				Row row5f = sheet3.createRow((short) x + 31);
				Row row7f = sheet3.createRow((short) x + 46);
				Row row9f = sheet3.createRow((short) x + 61);
				Row row1g = sheet3.createRow((short) x + 76);
				Row row3g = sheet3.createRow((short) x + 91);
				Row row5g = sheet3.createRow((short) x + 106);

				int j = 0;
				for (j = c; j <= d; j++) {
					//System.out.print(d);
					row1f.createCell(j - (x * fn)).setCellValue(finaloutput.get(0).AvgfirmK.get(j));
					row3f.createCell(j - (x * fn)).setCellValue(finaloutput.get(0).Avgfirmmainbank.get(j));
					row5f.createCell(j - (x * fn)).setCellValue(finaloutput.get(0).Avgfirmleverage.get(j));
					row7f.createCell(j - (x * fn)).setCellValue(finaloutput.get(0).SDfirmmainbank.get(j));
					row9f.createCell(j - (x * fn)).setCellValue(finaloutput.get(0).Avgfirmgrowthrate.get(j));
					row1g.createCell(j - (x * fn)).setCellValue(finaloutput.get(0).AvgfirmlevafterCM.get(j));
					row3g.createCell(j - (x * fn)).setCellValue(finaloutput.get(0).Avgfirminmarketstep.get(j));
					row5g.createCell(j - (x * fn)).setCellValue(FOFdeg.get(j).getAvgDegree());
					//row7.createCell(j-(x*fn)).setCellValue(e1.getFirmnextLeverage(j));
				}

				if (firmnode - 1 > fn + j) {
					if (firmnode - j - 1 > fn) {
						c = j;
						d = j + fn - 1;
					}
				} else {
					c = j;
					d = firmnode - 1;
				}
			}

			//各試行ごとの最大値
			/*
			Row row0x= sheet3.createRow((short)u+60);
			row0x.createCell(0).setCellValue("max取引先");
			for(int i=0;i<atp;i++) {
				Row row0y = sheet3.createRow((short)u+61);//行
				System.out.println(finaloutput.get(0).MaxMainbankNum.get(i));
				row0y.createCell(i).setCellValue(finaloutput.get(0).MaxMainbankNum.get(i));
			}
			*/

			//銀行

			for (int x = 0; x < 1; x++) {
				Row row0b = sheet2.createRow((short) x);
				Row row00b = sheet2.createRow((short) x);
				Row row1b = sheet2.createRow((short) x + 2);
				row1b.createCell(0).setCellValue("CAR");

				Row row2b = sheet2.createRow((short) x + 3);
				Row row3b = sheet2.createRow((short) x + 4);
				row3b.createCell(0).setCellValue("企業への貸付:IBloan:IBborrow");
				Row row4b = sheet2.createRow((short) x + 5);
				Row row5b = sheet2.createRow((short) x + 6);
				Row row6b = sheet2.createRow((short) x + 7);

				Row row7b = sheet2.createRow((short) x + 8);
				row7b.createCell(0).setCellValue("銀行資産規模K");
				Row row8b = sheet2.createRow((short) x + 9);
				Row row9b = sheet2.createRow((short) x + 10);

				row9b.createCell(0).setCellValue("銀行buffer");
				Row row10b = sheet2.createRow((short) x + 11);
				Row row11b = sheet2.createRow((short) x + 12);
				row11b.createCell(0).setCellValue("銀行総badloan");
				Row row12b = sheet2.createRow((short) x + 13);
				Row row13b = sheet2.createRow((short) x + 14);
				row13b.createCell(0).setCellValue("銀行residual");
				Row row14b = sheet2.createRow((short) x + 15);
				Row row15b = sheet2.createRow((short) x + 16);
				row15b.createCell(0).setCellValue("銀行総LFlistsize");
				Row row16b = sheet2.createRow((short) x + 17);
				Row row17b = sheet2.createRow((short) x + 18);
				row17b.createCell(0).setCellValue("銀行市場性資産");
				Row row18b = sheet2.createRow((short) x + 19);
				Row row19b = sheet2.createRow((short) x + 20);
				row19b.createCell(0).setCellValue("ClientNumber");
				Row row20b = sheet2.createRow((short) x + 21);

				for (int i = 0; i < banknode; i++) {
					row0b.createCell(i).setCellValue(i + 1);
					//row2.createCell(i).setCellValue(e1.getBankCAR(i));
					row4b.createCell(i).setCellValue(finaloutput.get(0).Avgbanklf.get(i));
					//row5.createCell(i).setCellValue(e1.getBankLb(i));
					//row6.createCell(i).setCellValue(e1.getBankBb(i));

					row8b.createCell(i).setCellValue(finaloutput.get(0).Avgbanktotalassets.get(i));

					//row10.createCell(i).setCellValue(e1.getBankBuffer(i));
					//row12.createCell(i).setCellValue(e1.getBankGrossBadLoan(i));
					row14b.createCell(i).setCellValue(finaloutput.get(0).Avgbankresidual.get(i));
					//row16.createCell(i).setCellValue(e1.getBankLFSize(i));
					//row18.createCell(i).setCellValue(e1.getBankLiqAsset(i));
					row20b.createCell(i).setCellValue(finaloutput.get(0).Avgbankcnum.get(i));

				}
				/*
				for(int i=0;i<bank.get(0).levlist.size();i++) {
					row18.createCell(i).setCellValue(e1.getBanklevlevelvalue(i));
				}
				*/
			}

			book.write(fileOut);
			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

	private static void getValue(Experiment e1) {
		// TODO 自動生成されたメソッド・スタブ

		ArrayList<Double> K = new ArrayList<Double>();
		ArrayList<Double> Badloan = new ArrayList<Double>();
		ArrayList<Double> Residual = new ArrayList<Double>();
		ArrayList<Double> Buffer = new ArrayList<Double>();
		ArrayList<Double> L = new ArrayList<Double>();

		//	kk+=e1.getBankK();
	}

	private static void showInitExcel(ArrayList<Institution> bank, ArrayList<Enterprise> firm) {

		Workbook book = null;

		try {
			book = new HSSFWorkbook();
			FileOutputStream fileOut = new FileOutputStream("workbookInitEx.xls");
			String safename = WorkbookUtil.createSafeSheetName("[InitBank]");
			String safename1 = WorkbookUtil.createSafeSheetName("[InitFirm]");
			Sheet sheet1 = book.createSheet(safename);
			Sheet sheet2 = book.createSheet(safename1);
			CreationHelper createHelper = book.getCreationHelper();

			//銀行
			int x = 0;

			Row row0 = sheet1.createRow((short) x);
			Row row1 = sheet1.createRow((short) x + 2);
			row1.createCell(0).setCellValue("CAR");
			Row row2 = sheet1.createRow((short) x + 3);
			Row row3 = sheet1.createRow((short) x + 4);
			row3.createCell(0).setCellValue("総資産");
			Row row4 = sheet1.createRow((short) x + 5);
			Row row5 = sheet1.createRow((short) x + 6);
			row5.createCell(0).setCellValue("Buffer");
			Row row6 = sheet1.createRow((short) x + 7);
			Row row7 = sheet1.createRow((short) x + 8);
			row7.createCell(0).setCellValue("初期企業取引先");
			Row row8 = sheet1.createRow((short) x + 9);
			Row row9 = sheet1.createRow((short) x + 10);
			row9.createCell(0).setCellValue("初期asset");
			Row row10 = sheet1.createRow((short) x + 11);
			Row row11 = sheet1.createRow((short) x + 12);
			row11.createCell(0).setCellValue("Inidnum");
			Row row12 = sheet1.createRow((short) x + 13);
			Row row13 = sheet1.createRow((short) x + 14);
			row13.createCell(0).setCellValue("Outidnum");
			Row row14 = sheet1.createRow((short) x + 15);

			for (Institution b : bank) {
				row0.createCell(b.id).setCellValue(b.id);
				row2.createCell(b.id).setCellValue(b.CAR);
				row4.createCell(b.id).setCellValue(b.totalassets);
				row6.createCell(b.id).setCellValue(b.buffer);
				row8.createCell(b.id).setCellValue(b.initclientnum);
				row10.createCell(b.id).setCellValue(b.Exasset.size());
				row12.createCell(b.id).setCellValue(b.Inid.size());
				row14.createCell(b.id).setCellValue(b.Outid.size());

			}

			book.write(fileOut);
			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}

	private static void showMultiExcel(Experiment e1, double kk, double car, double badloan, double residual, double l,
			int atp) {
		// TODO 自動生成されたメソッド・スタブ

		final String INPUT_DIR = "D:/Users/atsushi/Desktop/実験結果/";
		Workbook book = null;

		try {
			book = new HSSFWorkbook();
			FileOutputStream fileOut = new FileOutputStream("workbook1.xls");
			String safename = WorkbookUtil.createSafeSheetName("[OutPut+Firm]");
			String safename1 = WorkbookUtil.createSafeSheetName("[Bank]");
			Sheet sheet1 = book.createSheet(safename);
			Sheet sheet2 = book.createSheet(safename1);
			CreationHelper createHelper = book.getCreationHelper();

			int x = 1;

			Row row1 = sheet2.createRow((short) x + 2);
			row1.createCell(0).setCellValue("CAR");
			Row row2 = sheet2.createRow((short) x + 3);
			row2.createCell(0).setCellValue(car / atp);
			Row row3 = sheet2.createRow((short) x + 4);
			row3.createCell(0).setCellValue("企業への貸付");
			Row row4 = sheet2.createRow((short) x + 5);
			row4.createCell(0).setCellValue(l / atp);
			Row row5 = sheet2.createRow((short) x + 5);
			row3.createCell(0).setCellValue("badloan");
			//Row row5=sheet2.createRow((short)x+5);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

	private static void showExcel(Experiment e1, ArrayList<Institution> bank) {
		// TODO Auto-generated method stub

		final String INPUT_DIR = "D:/Users/atsushi/Desktop/実験結果/";
		Workbook book = null;

		try {
			book = new HSSFWorkbook();
			FileOutputStream fileOut = new FileOutputStream("workbooktime.xls");
			String safename = WorkbookUtil.createSafeSheetName("[OutPut]");
			String safename1 = WorkbookUtil.createSafeSheetName("[Bank]");
			String safename2 = WorkbookUtil.createSafeSheetName("[Firm]");
			Sheet sheet1 = book.createSheet(safename);
			Sheet sheet2 = book.createSheet(safename1);
			Sheet sheet3 = book.createSheet(safename2);
			CreationHelper createHelper = book.getCreationHelper();

			//100タームごとに改行
			int rowterm = e1.getTerm() / 100 + 1;
			int term = e1.getTerm();
			int xterm = 100;
			int a = 0, b = xterm - 1;
			//if(e1.getTerm()>100) {xterm=100;a=0;b=xterm-1;}else {xterm=0;a=0;b=e1.getTerm()-1;}
			for (int x = 0; x < rowterm; x++) {
				Row row = sheet1.createRow((short) x);//行
				Row row1 = sheet1.createRow((short) (x + rowterm + 2));
				Row row2 = sheet1.createRow((short) (x + rowterm * 2 + 2));
				Row row3 = sheet1.createRow((short) (x + rowterm * 3 + 2));
				Row row4 = sheet1.createRow((short) (x + rowterm * 4 + 2));
				Row row5 = sheet1.createRow((short) (x + rowterm * 5 + 2));
				Row row6 = sheet1.createRow((short) (x + rowterm * 6 + 2));
				Row row7 = sheet1.createRow((short) (x + rowterm * 7 + 2));
				Row row8 = sheet1.createRow((short) (x + rowterm * 8 + 2));
				Row row9 = sheet1.createRow((short) (x + rowterm * 9 + 2));
				Row row10 = sheet1.createRow((short) (x + rowterm * 10 + 2));
				Row row11 = sheet1.createRow((short) (x + rowterm * 11 + 2));
				Row row12 = sheet1.createRow((short) (x + rowterm * 12 + 2));
				Row row13 = sheet1.createRow((short) (x + rowterm * 11 + 2));

				int j = 0;
				for (j = a; j <= b; j++) {
					row.createCell(j - (x * xterm)).setCellValue(e1.getAgrregateOP(j));
					row1.createCell(j - (x * xterm)).setCellValue(e1.getGrowthRateOP(j));
					row2.createCell(j - (x * xterm)).setCellValue(e1.getFailedFirm(j));
					row3.createCell(j - (x * xterm)).setCellValue(e1.getFailedFirmrate(j));
					row4.createCell(j - (x * xterm)).setCellValue(e1.getBadLoan(j));
					row5.createCell(j - (x * xterm)).setCellValue(e1.getAVGInterest(j));
					row6.createCell(j - (x * xterm)).setCellValue(e1.getMoneyStock(j));
					row7.createCell(j - (x * xterm)).setCellValue(e1.getCreditMoney(j));
					row8.createCell(j - (x * xterm)).setCellValue(e1.getResidual(j));
					row9.createCell(j - (x * xterm)).setCellValue(e1.getGrossDemand(j));
					row10.createCell(j - (x * xterm)).setCellValue(e1.getGrossSupply(j));
					row11.createCell(j - (x * xterm)).setCellValue(e1.getRejectFinancing(j));

					//cell.setCellValue(createHelper.createRichTextString("sample String"));
				}
				/*
				for(j=a;j<e1.getAliveBank();j++) {
					rowx.createCell(j-(x*e1.getAliveBank())).setCellValue(e1.getBankCAR(j));
				}*/
				if (term > 100) {
					if (term - 1 > xterm + j) {
						if (term - j - 1 > xterm) {
							a = j;
							b = j + xterm - 1;
						}
					} else {
						a = j;
						b = term - 1;
					}

				}
			}

			//企業list
			e1.setSortFirmK();
			int rowfirm = e1.getalivefirm() / 100;
			int firmnode = e1.getalivefirm();
			int fn = 100;
			int c = 0, d = fn - 1;
			int t = 0;

			Row row00 = sheet3.createRow((short) t);
			row00.createCell(0).setCellValue("総資産");
			Row row02 = sheet3.createRow((short) t + 15);
			row02.createCell(0).setCellValue("取引先");
			Row row04 = sheet3.createRow((short) t + 30);
			row04.createCell(0).setCellValue("Leverage");
			Row row06 = sheet3.createRow((short) t + 45);
			row06.createCell(0).setCellValue("ChoiveLeverage");
			Row row08 = sheet3.createRow((short) t + 60);
			row08.createCell(0).setCellValue("RejectOrder");
			Row row010 = sheet3.createRow((short) t + 75);
			row010.createCell(0).setCellValue("NonBuffer");
			for (int x = 0; x < rowfirm; x++) {
				/*
					Row row0= sheet3.createRow((short)x);
					row0.createCell(0).setCellValue("総資産");
					*/
				Row row1 = sheet3.createRow((short) x + 1);//行
				Row row3 = sheet3.createRow((short) x + 16);
				Row row5 = sheet3.createRow((short) x + 31);
				Row row7 = sheet3.createRow((short) x + 46);
				Row row9 = sheet3.createRow((short) x + 61);
				Row row11 = sheet3.createRow((short) x + 76);
				int j = 0;
				for (j = c; j <= d; j++) {
					//System.out.print(d);
					row1.createCell(j - (x * fn)).setCellValue(e1.getFirmK(j));
					row3.createCell(j - (x * fn)).setCellValue(e1.getFirmCandidateNum(j));
					row5.createCell(j - (x * fn)).setCellValue(e1.getFirmLeverage(j));
					row7.createCell(j - (x * fn)).setCellValue(e1.getFirmnextLeverage(j));
					row9.createCell(j - (x * fn)).setCellValue(e1.getFirmRejectOrderNum(j));
					row11.createCell(j - (x * fn)).setCellValue(e1.getFirmNonBufferNum(j));
				}

				if (firmnode - 1 > fn + j) {
					if (firmnode - j - 1 > fn) {
						c = j;
						d = j + fn - 1;
					}
				} else {
					c = j;
					d = firmnode - 1;
				}
			}
			/*
			if(rowfirm==0) {
				int x=0;
				Row row0= sheet3.createRow((short)x);
				row0.createCell(0).setCellValue("総資産");
				Row row1 = sheet3.createRow((short)x);//行
				Row row2=sheet3.createRow((short)x+15);
				row2.createCell(0).setCellValue("取引先");
				Row row3 = sheet3.createRow((short)x);
				Row row4=sheet3.createRow((short)x+30);
				row4.createCell(0).setCellValue("Leverate");
				Row row5 = sheet3.createRow((short)x);
				Row row6=sheet3.createRow((short)x+45);
				row6.createCell(0).setCellValue("ChoiveLeverage");
				Row row7 = sheet3.createRow((short)x);
				//Row row8 = sheet3.createRow((short)x);
				//Row row9=sheet3.createRow((short)x+20);
			for(int i=0;i<firmnode;i++) {
				row1.createCell(i).setCellValue(e1.getFirmK(i));
				row3.createCell(i).setCellValue(e1.getFirmCandidateNum(i));
				row5.createCell(i).setCellValue(e1.getFirmLeverage(i));
				row7.createCell(i).setCellValue(e1.getFirmnextLeverage(i));
				//row4.createCell(i).setCellValue(e1.getFirmLevNowEvalue(i));
			}
			}
			*/

			//銀行リスト

			int banknode = e1.getAliveBank();

			for (int x = 0; x < 1; x++) {
				Row row0 = sheet2.createRow((short) x);
				Row row1 = sheet2.createRow((short) x + 2);
				row1.createCell(0).setCellValue("CAR");
				Row row2 = sheet2.createRow((short) x + 3);
				Row row3 = sheet2.createRow((short) x + 4);
				row3.createCell(0).setCellValue("企業への貸付:IBloan:IBborrow");
				Row row4 = sheet2.createRow((short) x + 5);
				Row row5 = sheet2.createRow((short) x + 6);
				Row row6 = sheet2.createRow((short) x + 7);
				Row row7 = sheet2.createRow((short) x + 8);
				row7.createCell(0).setCellValue("銀行資産規模K");
				Row row8 = sheet2.createRow((short) x + 9);
				Row row9 = sheet2.createRow((short) x + 10);
				row9.createCell(0).setCellValue("銀行buffer");
				Row row10 = sheet2.createRow((short) x + 11);
				Row row11 = sheet2.createRow((short) x + 12);
				row11.createCell(0).setCellValue("銀行総badloan");
				Row row12 = sheet2.createRow((short) x + 13);
				Row row13 = sheet2.createRow((short) x + 14);
				row13.createCell(0).setCellValue("銀行residual");
				Row row14 = sheet2.createRow((short) x + 15);
				Row row15 = sheet2.createRow((short) x + 16);
				row15.createCell(0).setCellValue("銀行総LFlistsize");
				Row row16 = sheet2.createRow((short) x + 17);
				Row row17 = sheet2.createRow((short) x + 18);
				row17.createCell(0).setCellValue("銀行市場性資産");
				Row row18 = sheet2.createRow((short) x + 19);
				Row row19 = sheet2.createRow((short) x + 20);
				row19.createCell(0).setCellValue("ClientNumber");
				Row row20 = sheet2.createRow((short) x + 21);

				for (int i = 0; i < banknode; i++) {
					row0.createCell(i).setCellValue(e1.getBankId(i));
					row2.createCell(i).setCellValue(e1.getBankCAR(i));
					row4.createCell(i).setCellValue(e1.getBankLf(i));
					row5.createCell(i).setCellValue(e1.getBankLb(i));
					row6.createCell(i).setCellValue(e1.getBankBb(i));
					row8.createCell(i).setCellValue(e1.getBankK(i));
					row10.createCell(i).setCellValue(e1.getBankBuffer(i));
					row12.createCell(i).setCellValue(e1.getBankGrossBadLoan(i));
					row14.createCell(i).setCellValue(e1.getBankResidual(i));
					row16.createCell(i).setCellValue(e1.getBankLFSize(i));
					row18.createCell(i).setCellValue(e1.getBankLiqAsset(i));
					row20.createCell(i).setCellValue(e1.getBankClientNum(i));
				}
				/*
				for(int i=0;i<bank.get(0).levlist.size();i++) {
					row18.createCell(i).setCellValue(e1.getBanklevlevelvalue(i));
				}
				*/

			}
			book.write(fileOut);
			//fileOut.close();

			// 1つ目のセルを作成 ※行と同じく、0からスタート
			//Cell a1 = row.createCell(0);  // Excel上、「A1」の場所

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

}
