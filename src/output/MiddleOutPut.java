package output;

import java.util.ArrayList;

import bankagent.Institution;

public class MiddleOutPut {
	//middle bank output list

	public int id;
	public int atp;



	public ArrayList<Double> Avgtotalassets=new ArrayList<Double>();
	public ArrayList<Double> CAR=new ArrayList<Double>();
	public ArrayList<Double> Avgindegnum=new ArrayList<Double>();
	public ArrayList<Double> Avgoutdegnum=new ArrayList<Double>();
	public ArrayList<Double> Avgdegsum=new ArrayList<Double>();
	public ArrayList<Double> AvgliqA=new ArrayList<Double>();
	public ArrayList<Double> Avgbb=new ArrayList<Double>();
	public ArrayList<Double> Avglf=new ArrayList<Double>();
	public ArrayList<Double> Avglb=new ArrayList<Double>();
	public ArrayList<Double> Avgclientnum=new ArrayList<Double>();
	public ArrayList<Double> Avgdr=new ArrayList<Double>();
	public ArrayList<Double> Avgpr=new ArrayList<Double>();

	public ArrayList<Double> DR = new ArrayList<Double>();
	public ArrayList<Double> PR = new ArrayList<Double>();
	public ArrayList<Integer> Deg = new ArrayList<Integer>();
	public ArrayList<Double> RelAsset = new ArrayList<Double>();
	public ArrayList<Double> Assetsize = new ArrayList<Double>();
	public ArrayList<String> Rank = new ArrayList<String>();

	public double avgtotalassets;
	public double car;
	public double avgindegnum;
	public double avgoutdegnum;
	public double avgdegsum;
	public double avgliqA;
	public double avgbb;
	public double avglf;
	public double avgbuffer;
	public double avglb;
	public double avgclientnum;
	public double dr;
	public double pr;


	public MiddleOutPut(int id,int atp) {
		this.id=id;
		this.atp=atp;
	}



	public void setResultAndCalc(Institution b) {
		// TODO 自動生成されたメソッド・スタブ

		avgtotalassets+=b.totalassets/atp;
		car+=b.CAR/atp;
		avgindegnum+=(double)b.Inid.size()/atp;
		avgoutdegnum+=(double)b.Outid.size()/atp;
		avgdegsum+=(double)b.deg/atp;
		avgliqA+=b.liqA/atp;
		avgbb+=b.bb/atp;
		avglf+=b.lf/atp;
		avgbuffer+=b.buffer/atp;
		avglb+=b.lb/atp;
		avgclientnum+=(double)b.clientnum/atp;
		dr+=(double)b.dr/atp;
		pr+=(double)b.pr/atp;


		DR.add(b.dr);
		PR.add(b.pr);
		Deg.add(b.deg);
		RelAsset.add(b.rasize);
		Assetsize.add(b.totalassets);
		Rank.add(b.rank);
		/*
		Avgtotalassets.add(b.totalassets/atp);
		Avgindegnum.add(((double)b.Inid.size()/atp));
		Avgoutdegnum.add(((double)b.Outid.size()/atp));
		Avgtotalassets.add((double)b.deg/atp);
		AvgliqA.add(b.liqA/atp);
		Avgbb.add(b.bb/atp);
		Avglf.add(b.lf/atp);
		Avglb.add(b.lb/atp);
		Avgclientnum.add((double)b.clientnum/atp);
		*/
	}




}
