package bankagent;

public class IBShort implements Cloneable{

	public int id;//取引先id
	public double money;
	public double dif;//所望取引額との差分

	public double CAR;//オーダーしてきた先のcar

	public double indegw,outdegw;//IB全体に対する取引割合

	//create order construtor
	public IBShort(int id, double shortage) {
		this.id=id;
		this.money=shortage;
	}

	//received and response order constructor
	public IBShort(int i, double d,double car) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id=i;
		this.money=d;
		this.CAR=car;
	}


	public void addloanmoney(double amount) {
		money+=amount;
	}

	public void addborrowmoney(double amount) {
		money+=amount;
	}

	public void setIndegw(double d) {
		// TODO 自動生成されたメソッド・スタブ
		indegw=d;
	}

	public void setOutdegw(double d) {
		// TODO 自動生成されたメソッド・スタブ
		outdegw=d;
	}

	public void setBorrowDif(double ibbdes) {
		dif=ibbdes*indegw-money;
	}

	public void setLoanDif(double ibldes){
		// TODO 自動生成されたメソッド・スタブ
		dif=ibldes*outdegw-money;
	}

	public IBShort clone() {
		try {
			IBShort ibs=(IBShort) super.clone();
			return ibs;
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());

		}
	}


}
