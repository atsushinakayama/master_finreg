package firmagent;

import bankagent.LendingFirm;

public class Leverage implements Cloneable{


	public int levnum;//0-9

	public double level;//レバレッジレベル
	public double evalue;//評価値
	public int x;//ターム
	public String state;//選ばれている状態もしくは現在のレバレッジに近い値
	public String range;//選択可能範囲か
	public String adjacent="No";//近隣かどうか
	public String finalrange="No";//このレバレッジの中から次期レバレッジを選択する

	public double X;//評価値の強度
	public double c=0.1;
	public double pl;//選択される確率
	public double vi;//strength係数



	public Leverage(double level,double evalue,int x,String state,String range,int i,double vi) {
		this.level=level;
		this.evalue=evalue;
		this.x=x;
		this.state=state;
		this.range=range;
		this.levnum=i;
		this.vi=vi;
	}


	public void setStrength() {
		if(evalue<0)evalue=0;
		X=Math.pow(evalue/c,vi);


	}

	public void setAdjacent(String r) {
		adjacent=r;
	}

	public Leverage clone() {
		try {
			Leverage le=(Leverage) super.clone();
			return le;
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());

		}
	}


}
