package asset;

import java.util.ArrayList;

public class Asset implements Cloneable{

	public int id;
	public int group;
	public double vol;//ボラティリティ
	public double ascor;//相関
	public double flucrate;//変動率
	public double selledamount;//売却量合計
	public double boughtamount;//購入量合計
	public double totalamount;//全体の購入金額
	public double accumflucrate;//変動率累積
	public double accumselled;//変動率累積
	public double accumbought;//変動率累積
	public double curamount;//正味価格(売却時価格)
	public double groupfluc;//同グループの変動率

	public double dec;//shockを与える場合の下落率
	public double fsr;//銀行が破綻した際の固定propagate割合

	//時系列格納リスト
	//毎時
	ArrayList<Double> Fluc=new ArrayList<Double>();
	ArrayList<Double> SelledAmount=new ArrayList<Double>();
	ArrayList<Double> BoughtAmount=new ArrayList<Double>();
	//累積
	ArrayList<Double> AccumFluc=new ArrayList<Double>();
	ArrayList<Double> AccumSelled=new ArrayList<Double>();
	ArrayList<Double> AccumBought=new ArrayList<Double>();

	public Asset(int id, int group, double vol, double ascor, double fsr) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id;
		this.group = group;
		this.vol = vol;
		this.ascor = ascor;
		this.fsr = fsr;
	}


	public Asset clone() {
		try {
			Asset as=(Asset) super.clone();
			as.Fluc=new ArrayList<Double>(Fluc);
			as.SelledAmount=new ArrayList<Double>(SelledAmount);
			as.BoughtAmount=new ArrayList<Double>(BoughtAmount);
			as.AccumFluc=new ArrayList<Double>(AccumFluc);
			as.AccumSelled=new ArrayList<Double>(AccumSelled);
			as.AccumBought=new ArrayList<Double>(AccumBought);
			return as;
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());
		}
	}



	public void initGroupFluc() {
		groupfluc = 0;
	}

	public void setVolFlucrate() {
		flucrate += -vol + 2 * vol * Math.random()+dec;
	}

	public void setByGroupFluc(ArrayList<Asset> asset) {
		//add groupfluc
		for (Asset as : asset) {
			if (group == as.group && id != as.id) {
				groupfluc += ascor * as.flucrate;
			}
		}
		flucrate += groupfluc * ascor;
		groupfluc=0;
	}

	public void setShockFlucrate(double rate, double money) {
		//from BankruptProcess.java

		//rate:shockpropagateにおける変動幅の限界割合
		if(totalamount !=0) {
		flucrate -= rate * (money / totalamount);
		totalamount -= money;
		}else {
			totalamount = 0;
			flucrate = 0;
		}
	}

	public void setShockFlucrateStatic(double money) {
		flucrate -= fsr;
		totalamount -= money;
	}

	/*
	public void asGroupShockFluctuate(double rate, double money) {
		// TODO 自動生成されたメソッド・スタブ
		flucrate+=ascor*(money/totalamount);
	}
	*/

	public void setTotalAmount(double bankhasmoney) {
		//from Build.java
		totalamount += bankhasmoney;
	}

	public void cutTotalAmount(double cutamount) {
		totalamount -= cutamount;
	}

	public void setSellAmount(double sellamount) {
		// TODO 自動生成されたメソッド・スタブ
		selledamount += sellamount;
	}

	public void setBuyAmount(double buyamount) {
		// TODO 自動生成されたメソッド・スタブ
		boughtamount += buyamount;
	}

	public void setBuyAndSellFluc(double rate) {
		// TODO 自動生成されたメソッド・スタブ
		flucrate += rate * ((boughtamount / totalamount) - (selledamount / totalamount));
		totalamount += boughtamount - selledamount;
	}


	public void setTimeseriesData() {
		Fluc.add(flucrate);
		SelledAmount.add(selledamount);
		BoughtAmount.add(boughtamount);
	}

	public void setAccumurateData() {
		accumflucrate+=flucrate;
		accumselled+=selledamount;
		accumbought+=boughtamount;
		AccumFluc.add(accumflucrate);
		AccumSelled.add(accumselled);
		AccumBought.add(accumbought);
	}

	public void Init() {
		flucrate = 0;
		selledamount=0;
		boughtamount=0;
	}



}
