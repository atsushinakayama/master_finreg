package networkCP;

import java.util.ArrayList;
import java.util.Collections;

public class NetworkCP1 {

	/*
	 * create core-peripheral network
	 * create core-peripheral network is same degree with BA network
	 */

	public int banknode;
	public double a1, a2;//large,medium,small銀行の割合
	public ArrayList<Virtual> agent = new ArrayList<Virtual>();//ネットワーク作成用
	public ArrayList<Virtual> mega = new ArrayList<Virtual>();
	public ArrayList<Virtual> medium = new ArrayList<Virtual>();
	public ArrayList<Virtual> small = new ArrayList<Virtual>();
	public int minibCN = 4;//中小銀行グループ内での完全グラフの行数
	public double randsamegroup = 0.2;
	public double randIn, randOut;//つながっていない銀行とランダムに接続する確率
	//disassoratative
	public double dismega_medium = 0.3, dismega_small = 0.1, dismedium_small = 0.03;
	public double dis_cp = 0.1;
	double c1=0,c2=0;//ネットワーク次数カウンタ
	public int degsum;//create cp deg counter

	public boolean isCreate;// 枝を追加していくアルゴリズムで，degba<degsumであればfalseを返す

	public void createAgent(int banknode) {
		for (int i = 1; i <= banknode; i++) {
			Virtual vir = new Virtual(i, banknode);
			agent.add(vir);
			if (i <= banknode * a1) {
				agent.get(i - 1).rank = "mega";
				mega.add(agent.get(i - 1));
			} else {
				if (i <= banknode * a2) {
					agent.get(i - 1).rank = "medium";
					medium.add(agent.get(i - 1));
				} else {
					agent.get(i - 1).rank = "small";
					small.add(agent.get(i - 1));
				}
			}
		}
	}

	public NetworkCP1(int banknode, double a1, double a2, double randIn, double randOut) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.banknode = banknode;
		this.a1 = a1;
		this.a2 = a2;
		this.randIn = randIn;
		this.randOut = randOut;
		//仮想エージェントを作りネットワークを作成
		//ランク分け
		createAgent(banknode);
	}



	public void Disassoratative(ArrayList<Virtual> agent1, ArrayList<Virtual> agent2, double dis, int degba) {
		for (Virtual m : agent1) {
			if(degsum==degba) break;
			for (Virtual o : agent2) {
				double rndIn = Math.random();
				double rndOut = Math.random();
				if (!m.Inid.contains(o.id) && rndIn < dis) {
					m.Inid.add(o.id);
					o.Outid.add(m.id);
					degsum+=2;
				}
				if(degsum==degba) break;
				if (!m.Outid.contains(o.id) && rndOut < dis) {
					m.Outid.add(o.id);
					o.Inid.add(m.id);
					degsum+=2;
				}
				if(degsum==degba) break;
			}
		}
	}

	public void ConnectSameGroup(ArrayList<Virtual> agent1 , ArrayList<Virtual> agent2) {
		for(Virtual m: agent1) {
			for(Virtual o : agent2) {
				double rndIn = Math.random();
				double rndOut = Math.random();
				if(m.groupid==o.groupid && !m.Inid.contains(o.id) && rndIn < randsamegroup) {
					m.Inid.add(o.id);
					o.Outid.add(m.id);
				}
				if (m.groupid==o.groupid && !m.Outid.contains(o.id) && rndOut < randsamegroup) {
					m.Outid.add(o.id);
					o.Inid.add(m.id);
				}
			}
		}
	}

	public void connectPriority() {
		//総次数が大きいほど接続されやすい
		agent.stream().forEach(s->{
			c1+=s.Inid.size();
			c2+=s.Outid.size();
		});
		agent.stream().forEach(s->s.setDegPer(c1,c2));

		ArrayList<Virtual> skel=new ArrayList<Virtual>();
		skel.addAll(agent);
		Collections.shuffle(skel);
		for(Virtual s:skel) {

		}

	}

	public void connectRandom(int degba) {
		for (Virtual a : agent) {
			if(degsum==degba) break;
			for (Virtual o : agent) {
				double rndIn = Math.random();
				double rndOut = Math.random();
				if (a.id != o.id && !a.Inid.contains(o.id) && rndIn < randIn) {
					a.Inid.add(o.id);
					o.Outid.add(a.id);
					degsum+=2;
				}
				if(degsum==degba) break;
				if (a.id != o.id && !a.Outid.contains(o.id) && rndOut < randOut) {
					a.Outid.add(o.id);
					o.Inid.add(a.id);
					degsum+=2;
				}
				if(degsum==degba) break;
			}
		}
	}

	public void CreateCluster() {
		for (Virtual m : medium) {
			for (Virtual mm : medium) {
				if (m.groupid == mm.groupid && m.id != mm.id) {
					m.Inid.add(mm.id);
					m.Outid.add(mm.id);
				}
			}
		}
	}

	public void CreateGroup(int alloc, ArrayList<Virtual> agent, ArrayList<Virtual> mega) {
		int i = 1, counter = 0;
		for (Virtual s : agent) {
			s.groupid = i;
			counter++;
			if (counter == alloc) {
				i++;
				counter = 0;
			}
		}
		for (Virtual m : mega) {
			for (Virtual o : agent) {
				if (m.id == o.groupid) {
					m.Inid.add(o.id);
					m.Outid.add(o.id);
					o.Inid.add(m.id);
					o.Outid.add(m.id);
				}
			}
		}
	}

////network CP adding until same degree of BA////////////////////////////////////////////////////////////////////////////////////////////

	public boolean createCP_sameDegBA(int degba) {
				//大銀行完全ネットワークの作成
				for (Virtual m : mega) {
					for (Virtual mm : mega) {
						if (m.id != mm.id) {
							m.Inid.add(mm.id);
							m.Outid.add(mm.id);
						}
					}
				}
				//大銀行ー中・小銀行グループ化
				int mediumalloc = medium.size() / mega.size();//1グループあたりの中銀行
				int smallalloc = small.size() / mega.size();//1グループあたりの小銀行
				CreateGroup(mediumalloc, medium, mega);
				CreateGroup(smallalloc, small, mega);

				//中銀行クラスター化(medium)
				CreateCluster();

				//各グループ内の中銀行と小銀行をランダムで結ぶ
				ConnectSameGroup(medium,small);


				//degreeをBAに合わせる
				agent.stream().forEach(s->degsum+=s.Inid.size()+s.Outid.size());
				System.out.println("before : degba;"+degba+" degcp"+degsum);
				if(degsum<degba) {
					do {
						//random connect 優先的選択
						//connectPriority();

						//disassoratative
						Disassoratative(mega, medium, dismega_medium,degba);
						if(degsum==degba) break;
						Disassoratative(medium, small, dismedium_small,degba);
						if(degsum==degba) break;
						Disassoratative(mega, small, dismega_small,degba);
						if(degsum==degba) break;

						//randomに接続
						connectRandom(degba);
						if(degsum==degba) break;

					}while(degsum<degba);
					isCreate = true;
				}else{
					isCreate = false;
				}
				if(degsum!=degba) {
					System.out.println("not same deg ba"+degba+" degcp"+degsum);
				}

				return isCreate;
	}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public int getIndegree(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return agent.get(i).Inid.size();
	}

	public int getOutdegree(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return agent.get(i).Outid.size();
	}

	public ArrayList<Integer> getInid(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return agent.get(i).Inid;
	}

	public ArrayList<Integer> getOutid(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return agent.get(i).Outid;
	}

	public void virtualClear() {
		// TODO 自動生成されたメソッド・スタブ
		agent.clear();
		mega.clear();
		medium.clear();
		small.clear();
		degsum = 0;
	}

}
