import xlrd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

outputPath = "././Histgrams/"

def create_Histgram_alive(wbba,wbcp,pt):
    baal = wbba.sheet_by_name("残存数")
    cpal = wbcp.sheet_by_name("残存数")
    baalive = baal.col_values(0)
    cpalive = cpal.col_values(0)

    sns.set()
    sns.set_style('whitegrid')
    sns.set_palette('Set1')

    fig = plt.figure()
    ax  = fig.add_subplot(1, 1, 1)

    ax.hist([baalive,cpalive], label = ["BA","CP"],range = (0, 100), bins = 20, alpha = 0.7, density = True)
    ax_yticklocs = ax.yaxis.get_ticklocs()#目盛りの情報を取得
    ax_yticklocs = list(map(lambda x: x * len(range(0,100))*1.0/20, ax_yticklocs))#元の目盛りの値にbinの幅を掛ける
    ax.yaxis.set_ticklabels(list(map(lambda x: "%0.2f" % x, ax_yticklocs)))#直した目盛りを表示
    ax.set_xlabel("alive")
    ax.set_ylabel("frequet")
    plt.legend()
    plt.savefig(outputPath+'alive'+pt)
    plt.show()

def create_Histgram_totalloss(wbba,wbcp,pt):
    baal = wbba.sheet_by_name("totalloss")
    cpal = wbcp.sheet_by_name("totalloss")
    baloss = baal.col_values(0)
    cploss = cpal.col_values(0)

    sns.set()
    sns.set_style('whitegrid')
    sns.set_palette('Set1')

    fig = plt.figure()
    ax  = fig.add_subplot(1, 1, 1)

    ax.hist([baloss,cploss], label = ["BA","CP"], range = (0,5000), bins = 50,alpha = 0.7, density = True)
    ax_yticklocs = ax.yaxis.get_ticklocs()#目盛りの情報を取得
    ax_yticklocs = list(map(lambda x: x * len(range(0,5000))*1.0/50, ax_yticklocs))#元の目盛りの値にbinの幅を掛ける
    ax.yaxis.set_ticklabels(list(map(lambda x: "%0.2f" % x, ax_yticklocs)))#直した目盛りを表示
    ax.set_xlabel("losses")
    ax.set_ylabel("frequet")
    plt.legend()
    plt.savefig(outputPath+'loss'+pt)
    plt.show()
